<?php

use yii\helpers\Url;

class HomeCest
{
    public function testWorkHomePage(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/'));
        $I->see("Хиты продаж");
    }
}
