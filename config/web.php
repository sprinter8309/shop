<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$db_local = require __DIR__ . '/db_local.php';
$modules = require __DIR__ . '/modules.php';
$url_rules = require __DIR__ . '/url-rules.php';

$config = [
    'id' => 'basic',
    'name' => 'Интернет магазин SHOP',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules'=>$modules,
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@upload'   => '/web/upload'
    ],
    'charset' => 'UTF-8',
    'language' => 'ru_RU',
    'components' => [
        'request' => [
            'baseUrl'=>'',
            'cookieValidationKey' => 'C8eEzMDRnmo52rlc5U9roZhhvYdEcx0I'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true
        ],
        'errorHandler' => [
            'errorAction' => 'site/base/error'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning']
                ],
            ],
        ],
        'db' => $db_local,
        'user' => [
            'identityClass' => 'app\models\User'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => $url_rules
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => \yii\debug\Module::class,
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*', '::1'],
    ];
    
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*', '::1'],
    ];
}

return $config;
