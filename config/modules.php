<?php

use app\modules\news\NewsModule;
use app\modules\site\SiteModule;
use app\modules\catalog\CatalogModule;
use app\modules\cart\CartModule;
use app\modules\order\OrderModule;
use app\modules\user\UserModule;

return [
    'news'=>[
        'class' => NewsModule::class,
        'layout' => '@app/modules/site/views/layouts/base'
    ],
    'site'=>[
        'class' => SiteModule::class,
        'layout' => '@app/modules/site/views/layouts/base'
    ],
    'catalog'=>[
        'class' => CatalogModule::class,
        'layout' => '@app/modules/site/views/layouts/base'
    ],
    'cart'=>[
        'class' => CartModule::class,
        'layout' => '@app/modules/site/views/layouts/base'
    ],
    'order'=>[
        'class' => OrderModule::class,
        'layout' => '@app/modules/site/views/layouts/base'
    ],
    'user'=>[
        'class' => UserModule::class,
        'layout' => '@app/modules/site/views/layouts/base'
    ]
];
