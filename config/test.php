<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/test_db.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'basic-tests',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'en-US',
    'components' => [
        'request' => [
            'baseUrl'=>'',
            'cookieValidationKey' => 'C8eEzMDRnmo52rlc5U9roZhhvYdEcx0I'
        ],
        'db' => $db,
        'mailer' => [
            'useFileTransport' => true,
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

                '<controller:(catalog|news|article|contact)>'=>'<controller>/index',

                'catalog/item/<id:\d+>'=>'catalog/view',
                'catalog/<category>'=>'catalog/category',
                'catalog/item/img/<id:\d+>'=>'catalog/image',
                'catalog/category/<category>'=>'catalog/index',
                'sale/add/<id:\d+>'=>'sale/add',
                'sale/delete/<id:\d+>'=>'sale/delete',
                'cart'=>'sale/cart',
                'order'=>'sale/order',

                'news/<id:\d+>'=>'news/view',
                'news/<action:\w+>'=>'news/<action>',
                'news/img/<id:\d+>'=>'news/image',
                'article/<action:\w+>'=>'article/<action>',

                '<action:(agreement|policy|pay-info)>'=>'contact/<action>',
                '<action:(login|register|logout|cabinet)>'=>'user/<action>',
                'cabinet/<action:\w+>'=>'user/<action>',
                '<action:\w+>'=>'base/<action>',
                ''=>'base/index'
            ]
//            'showScriptName' => true,
        ],
        'user' => [
            'identityClass' => 'app\models\User',
        ],
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
        ],
    ],
    'params' => $params,
];
