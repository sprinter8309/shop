<?php

return [
    [
        'class'       => yii\web\GroupUrlRule::class,
        'prefix'      => '',
        'routePrefix' => 'site/base',
        'rules'       => [
            'GET /'                                                             => 'index',
            'GET contact'                                                       => 'contact',
            'GET policy'                                                        => 'policy',
            'GET agreement'                                                     => 'agreement',
            'GET pay-info'                                                      => 'pay-info',
            'GET delivery'                                                      => 'delivery',
        ]
    ],
    [
        'class'       => yii\web\GroupUrlRule::class,
        'prefix'      => 'site',
        'routePrefix' => 'site/base',
        'rules'       => [
            'GET wb-image/news_images/<file_name:[\w\d]+>'                      => 'news-image',
            'GET wb-image/item_images/<file_name:[\w\d]+>'                      => 'item-image',
            'GET wb-image/item_images/<[\w\d]+>/<file_name:[\w\d]+>/'                      => 'item-image',
            'GET wb-image/item_images/<c:[\w\d]+>/<file_name:[\w\d]+>/'                    => 'item-image',
            'GET wb-image/item_images/<c:[\w\d]+>/<sc:[\w\d]+>/<file_name:[\w\d]+>/'       => 'item-image',
        ]
    ],
    [
        'class'       => yii\web\GroupUrlRule::class,
        'prefix'      => 'news',
        'routePrefix' => 'news/base',
        'rules'       => [
            'GET /'                                                     => 'index',
            'GET <id:[\d]+>'                                            => 'view'
        ]
    ],
    [
        'class'       => yii\web\GroupUrlRule::class,
        'prefix'      => 'catalog',
        'routePrefix' => 'catalog/base',
        'rules'       => [
            'GET /'                                                     => 'index',
            'GET category/<category:[\w\d\-]+>'                         => 'index',
            'GET <category:[\w\d\-]+>'                                  => 'category',
            'GET item/<id:[\w\d\-]+>'                                   => 'view',
        ]
    ],
    [
        'class'       => yii\web\GroupUrlRule::class,
        'prefix'      => 'cart',
        'routePrefix' => 'cart/base',
        'rules'       => [
            'GET /'                                                     => 'cart',
            'GET add/<id:[\d]+>'                                        => 'add',
            'GET delete/<id:[\d]+>'                                     => 'delete',
        ]
    ],
    [
        'class'       => yii\web\GroupUrlRule::class,
        'prefix'      => 'order',
        'routePrefix' => 'order/base',
        'rules'       => [
            'GET /'                                                     => 'order-base',
            'POST /'                                                    => 'order-send',
        ]
    ],
    [
        'class'       => yii\web\GroupUrlRule::class,
        'prefix'      => '',
        'routePrefix' => 'user/base',
        'rules'       => [
            'GET login'                                                     => 'login',
            'POST login'                                                    => 'login',
            'GET register'                                                  => 'register',
            'POST register'                                                 => 'register',
            'GET cabinet/update'                                            => 'update',
            'POST cabinet/update'                                           => 'update',
            'GET cabinet'                                                   => 'cabinet',
            'GET cabinet/orders'                                            => 'orders',
            'GET logout'                                                    => 'logout',
        ]
    ],

    'list/update'=>'catalog/base/update',

    '<module:\w+>' => '<module>/base/index',
    '<module:\w+>/<action:\w+>' => '<module>/base/<action>',
];
