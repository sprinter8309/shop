<?php

namespace app\helpers;

use DateTime;
use DateTimeZone;
use Exception;

class DateHelper
{
    /**
     * @param string $timezone
     *
     * @return string
     * @throws Exception
     */
    public static function now($timezone = 'UTC'): string
    {
        $timezone = $timezone ?? date_default_timezone_get();
        return (new DateTime('now', new DateTimeZone($timezone)))->format('Y-m-d H:i:s');
    }

    /**
     * Ответ в секундах
     * todo возможно расширить до миллисекунд?
     *
     * @param string $newDateTime
     * @param string $oldDateTime
     *
     * @return float|int
     */
    public static function diff(string $newDateTime, string $oldDateTime)
    {
        $newDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $newDateTime);
        $oldDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $oldDateTime);

        return abs($newDateTime->getTimestamp() - $oldDateTime->getTimestamp());
    }

    /**
     * Разница между двумя датами в днях ($period)
     *
     * @param $dateStart
     * @param $dateEnd
     * @param string $period
     *
     * @return \DatePeriod
     * @throws Exception
     */
    public function getPeriod($dateStart, $dateEnd, string $period = '1 day'): \DatePeriod
    {
        $begin = new DateTime($dateStart);
        $end = new DateTime($dateEnd);

        $interval = \DateInterval::createFromDateString($period);
        return new \DatePeriod($begin, $interval, $end);
    }

    /**
     * @param $date
     * @param string $modify
     * @param null|string $timezone
     *
     * @return string
     * @throws Exception
     */
    public static function getModifyDate($date, $modify = '-1 week', $timezone = 'UTC'): string
    {
        $timezone = $timezone ?? date_default_timezone_get();
        $date = new DateTime($date, new DateTimeZone($timezone));
        $date->modify($modify);
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * @param $date
     * @param string $format
     *
     * @return string
     * @throws Exception
     */
    public static function formatDate($date, $format = 'd.m.Y'): string
    {
        $date = new DateTime($date);
        return $date->format($format);
    }

    /**
     * @param $unixTimestamp
     * @param null|string $timezone
     *
     * @return DateTime
     * @throws Exception
     */
    public static function getDateTimeByUnix($unixTimestamp, $timezone = 'UTC'): DateTime
    {
        $timezone = $timezone ?? date_default_timezone_get();
        $date = new DateTime('now', new DateTimeZone($timezone));
        $date->setTimestamp($unixTimestamp);
        return $date;
    }

    /**
     * @param $date
     * @param string $timezone
     *
     * @return DateTime
     * @throws Exception
     */
    public static function createDate($date, $timezone = 'UTC'): DateTime
    {
        $timezone = $timezone ?? date_default_timezone_get();
        return (new DateTime($date, new DateTimeZone($timezone)));
    }

    /**
     * strtotime() will convert a string WITHOUT a timezone indication as if the string is a time in the
     * default timezone ( date_default_timezone_set() ).
     * So converting a UTC time like '2018-12-06T09:04:55' with strtotime() actually yields a wrong result.
     *
     * @param $date
     * @param string $timezone
     *
     * @return int
     * @throws Exception
     */
    public static function strtotime($date, $timezone = 'UTC'): int
    {
        return self::createDate($date, $timezone)->getTimestamp();
    }

    /**
     * @param $date
     * @param $timezone - old timezone
     * @param string $newTimezone - old timezone
     *
     * @return DateTime
     * @throws Exception
     */
    public static function createDateChangeTimezone($date, $timezone = 'UTC', $newTimezone = 'UTC'): DateTime
    {
        $date = self::createDate($date, $timezone);
        $date->setTimezone(new DateTimeZone($newTimezone));
        return $date;
    }
}
