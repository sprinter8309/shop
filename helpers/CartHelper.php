<?php

namespace app\helpers;

use Yii;

/**
 * Класс хэлпер для работы с корзиной
 * 
 * @author Oleg Pyatin
 */
class CartHelper
{
    public static function countCartItems(): int
    {
        $products = Yii::$app->session->get('products') ?? [];
        
        if (count($products)>0) {

            $count_products = 0;

            foreach($products as $id => $quantity) {
                $count_products += $quantity;
            }

            return $count_products;

        } else {

            return 0;
        }
    }
}
