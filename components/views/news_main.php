<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="widget-new-container">
    <?php foreach($latest_news as $news_item): ?>

        <div class="widget-news-item clearfix">
            <div class="widget-news-item-image">
                <?= Html::a(Html::img( '/site/wb-image/'.(explode('.', $news_item['image'])[0])),
                                Url::to(['/news/'.$news_item['id']])); ?>
            </div>
            <div class="widget-news-item-block">
                <?= Html::a($news_item['name'], Url::to(['/news/'.$news_item['id']]), ['class'=>'widget-news-item-link']); ?>
                <div class="widget-news-item-date">
                    25 Января 2018
                </div>
                <div class="widget-news-item-text">
                    <?= $news_item['preview_text'] ?>
                </div>
            </div>
        </div>

    <?php endforeach; ?>
</div>

<div class="main-popular-items-link">
    <?= Html::a( 'Смотреть все новости', Url::to(['/news']), ['class'=>'main-popular-link-button']); ?>
</div>
