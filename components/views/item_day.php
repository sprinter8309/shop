<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="widget-item-day-container">
    <div class="widget-item-day-block">
        <div class="widget-item-day-image">
            <?= Html::a( Html::img('/site/wb-image/'.(explode('.', $item->image)[0])),
                            Url::to(['/catalog/item/'.$item->id])); ?>
        </div>
        <div class="widget-item-day-point-name">
            <?= Html::a($item->name, Url::to(['/catalog/item/'.$item->id])); ?>
        </div>
        <div class="widget-item-day-point-text">
            Краткое описание
            <?= Html::a(' Подробнее...', Url::to(['/catalog/item/'.$item->id])); ?>
        </div>
        <div class="widget-item-day-price">
            <?= $item->price ?> р.
        </div>
    </div>
</div>

<div class="main-popular-items-link">
    <?= Html::a('Перейти к товару', Url::to(['/catalog/item/'.$item->id ]), ['class'=>'main-popular-link-button']); ?>
</div>
