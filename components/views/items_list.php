<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="widget-items-container clearfix">
    <div class="widget-item-point">
        <div class="widget-item-point-image">
            <?= Html::a( Html::img('/site/wb-image/'.(explode('.', $items[0]->image)[0])),
                            Url::to(['/catalog/item/'.$items[0]->id])); ?>
        </div>
        <div class="widget-item-point-name">
            <?= Html::a($items[0]->name, Url::to(['/catalog/item/'.$items[0]->id])); ?>
        </div>
        <div class="widget-item-point-text">
            Краткое описание
            <?= Html::a(' Подробнее...', Url::to(['/catalog/item/'.$items[0]->id])); ?>
        </div>
        <div class="widget-item-point-price">
            <?= $items[0]->price ?> р.
        </div>
    </div>
    <div class="widget-item-point">
        <div class="widget-item-point-image">
            <?= Html::a( Html::img('/site/wb-image/'.(explode('.', $items[1]->image)[0])),
                            Url::to(['/catalog/item/'.$items[1]->id])); ?>
        </div>
        <div class="widget-item-point-name">
            <?= Html::a($items[1]->name, Url::to(['/catalog/item/'.$items[1]->id])); ?>
        </div>
        <div class="widget-item-point-text">
            Краткое описание
            <?= Html::a(' Подробнее...', Url::to(['/catalog/item/'.$items[1]->id])); ?>
        </div>
        <div class="widget-item-point-price">
            <?= $items[1]->price ?> р.
        </div>
    </div>
    <div class="widget-item-point">
        <div class="widget-item-point-image">
            <?= Html::a( Html::img('/site/wb-image/'.(explode('.', $items[2]->image)[0])),
                            Url::to(['/catalog/item/'.$items[2]->id])); ?>
        </div>
        <div class="widget-item-point-name">
            <?= Html::a($items[2]->name, Url::to(['/catalog/item/'.$items[2]->id])); ?>
        </div>
        <div class="widget-item-point-text">
            Краткое описание
            <?= Html::a(' Подробнее...', Url::to(['/catalog/item/'.$items[2]->id])); ?>
        </div>
        <div class="widget-item-point-price">
            <?= $items[2]->price ?> р.
        </div>
    </div>
</div>
