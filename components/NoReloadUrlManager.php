<?php

namespace app\components;

use yii\web\UrlManager;

class NoReloadUrlManager extends UrlManager
{
    public function init()
    {
        parent::init();
    }

    public function createUrl(mixed $params): string
    {
        $sliced_path = preg_replace("/\/[^\/]+$/", '', $params['path']);

        return '/list/update?path='.$sliced_path.'/'.($params['page']-1);
    }
}

