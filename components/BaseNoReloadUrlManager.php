<?php

namespace app\components;

use yii\web\UrlManager;

class BaseNoReloadUrlManager extends UrlManager
{
    public function createUrl(mixed $params): string
    {
        $url_parts = explode('/', $params[0]);
        return '/list/update?path='.$url_parts[0].'/'.$params['category'].'/'.($params['page']-1);
    }
}