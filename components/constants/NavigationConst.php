<?php

namespace app\components\constants;

/**
 * Файл для вспомогательных констант навигации
 *
 * @author Oleg Pyatin
 */
class NavigationConst
{
    /**
     * URL страницы после успешного аутентификации
     */
    public final const URL_AFTER_LOGIN = '/cabinet';
    /**
     * URL страницы после выхода из системы
     */
    public final const URL_AFTER_LOGOUT = '/login';
    /**
     * URL страницы авторизации
     */
    public final const URL_LOGIN = '/login';
}