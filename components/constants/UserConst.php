<?php

namespace app\components\constants;

/**
 * Файл для констант работы с пользователями
 *
 * @author Oleg Pyatin
 */
class UserConst
{
    /**
     * Статус активного пользователя
     */
    public final const USER_STATUS_ACTIVE = 1;
    /**
     * Текст ошибки при регистрации пользователя
     */
    public final const ERROR_MESSAGE_REGISTER_USER = 'Возникла ошибка при регистрации ваших данных';
    /**
     * Текст ошибки при редактировании пользователя
     */
    public final const ERROR_MESSAGE_UPDATE_USER = 'Возникла ошибка при редактировании данных пользователя';
    /**
     * Обозначение успешного редактирования данных пользователя
     */
    public final const SUCCESS_UPDATE_USER = 'success_user_update';    
}