<?php

namespace app\components\constants;

/**
 * Файл для вспомогательных констант вывода
 *
 * @author Oleg Pyatin
 */
class OrderConst
{
    /**
     * Статус заказа ожидающего обработки
     */
    public final const ORDER_STATUS_PROCESSING_WAIT = 100;
    
    /**
     * Сообщение при внутренней ошибке сохранения
     */
    public final const ORDER_ERROR_SAVE_MESSAGE = 'Возникли непредвиденные ошибки при сохранении заказа, пожалуйста обратитесь к менеджеру';
}
