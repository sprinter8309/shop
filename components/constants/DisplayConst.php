<?php

namespace app\components\constants;

use app\components\enums\CatalogShowType;

/**
 * Файл для вспомогательных констант вывода
 *
 * @author Oleg Pyatin
 */
class DisplayConst
{
    /**
     * Количество выводимых новостей на главной странице этого раздела
     */
    public final const NEWS_LIST_QUANTITY = 20;
    /**
     * Количество выводимых новостей на главной странице сайта
     */
    public final const INDEX_PAGE_NEWS_LIST_QUANTITY = 3;
    
    /**
     * Количество выводимых категорий товаров на странице категорий
     */
    public final const CATEGORY_ITEM_LIST_QUANTITY = 20;
    
    /**
     * Базовое количество выводимых товаров для каталога
     */
    public final const DEFAULT_PAGINATION_SIZE = 20;
    /**
     * Базовый метод сортировки товаров
     */
    public final const DEFAULT_SORT_METHOD = 'price';
    /**
     * Базовый тип вывода (плитка или строками)
     */
    public final const DEFAULT_SHOW_TYPE = CatalogShowType::Tile;
    
    /**
     * Тип обновления каталога - способ вывода (плитка, строки)
     */
    public final const UPDATE_CATALOG_DISPLAY_TYPE = 'request-display';
    /**
     * Тип обновления каталога - пагинация (изменение количества выводимых товаров)
     */
    public final const UPDATE_CATALOG_PAGINATION_TYPE = 'request-pagination';
    /**
     * Тип обновления каталога - сортировка (по цене, по рейтингу)
     */
    public final const UPDATE_CATALOG_SORT_TYPE = 'request-sort';
    /**
     * Тип обновления каталога - фильтрация (например по-производителю)
     */
    public final const UPDATE_CATALOG_FILTER_TYPE = 'request-filter';
    /**
     * Тип обновления каталога - перелистывание (смотреть следующие товары в искомой категории)
     */
    public final const UPDATE_CATALOG_PAGE_TYPE = 'request-page';
    /**
     * Случай когда возникла какая-то ошибка и мы не делаем изменения в каталоге
     */
    public final const UPDATE_CATALOG_CANCEL_TYPE = 'request-no-update';
}
