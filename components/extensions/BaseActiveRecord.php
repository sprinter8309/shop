<?php

namespace app\components\extensions;

use app\components\exceptions\UnSuccessModelException;
use yii\db\ActiveRecord;

class BaseActiveRecord extends ActiveRecord
{
    /**
     * Функция безопасного сохранения с контролем успешности результата
     * 
     * @param null $attributeNames
     * @return bool
     * @throws UnSuccessModelException
     */
    public function safeSave($attributeNames = null): bool
    {
        $result = $this->save(true, $attributeNames);

        if ($this->hasErrors()) {
            throw new UnSuccessModelException($this->getErrorSummary(true)[0], $this);
        }

        return $result;
    }
}
