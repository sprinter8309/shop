<?php

namespace app\components;

class BaseLibrary 
{
    // Функция получения текстового сообщения для статуса
    public static function getStatusText($status) 
    {
        if ($status === 100) {
            return 'Ожидает обработки';
        }
    }
}
