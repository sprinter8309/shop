<?php

namespace app\components;
use yii\base\Widget;
use app\models\Items;

class ItemDay extends Widget
{
    public function run(): string
    {
        $item = Items::findOne( 14 );

        return $this->render('item_day', [
            'item' => $item
        ]);
    }
}
