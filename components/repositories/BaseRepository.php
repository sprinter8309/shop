<?php

namespace app\components\repositories;

use app\components\extensions\BaseActiveRecord;
use yii\base\BaseObject;

class BaseRepository extends BaseObject
{
    /**
     * Метод безопасного сохранения для модели
     * 
     * @param BaseAR $model
     * @throws exceptions\UnSuccessModelException
     */
    public function save(BaseActiveRecord $model): void
    {
        $model->safeSave();
        $model->refresh();
    }

    public function softDeleteElementById(string $entity_name, string $id): void
    {
        $delete_element = $entity_name::find(['id'])->where(['id'=>$id])->one();
        
        if ($delete_element) {
            $delete_element->is_deleted = 1;
            $delete_element->safeSave();
        }
    }
}
