<?php

namespace app\components\image_manager;

use Yii;
use yii\base\Component;
use yii\helpers\FileHelper;
use app\components\image_manager\entities\CreatingImageInfo;

/**
 * Компонент для выполнения действий над изображениями
 *
 * @author Oleg Pyatin
 */
class ImageManager extends Component
{
    protected const WHITE_BACKGROUND_WIDTH = 300;
    
    protected const WHITE_BACKGROUND_HEIGHT = 300;
    
    public function __construct(protected CreatingImageInfo $creating_image_info)
    {
        $this->creating_image_info = new CreatingImageInfo();
    }

    /**
     * Получение изображения нормированногно в квадрат в белом фоне 
     *
     * @param  string  $sir_path  Путь к директории файлов
     * @param  string  $file_name  Имя файла изображения для вывода
     * @return  void  Изображение будет отправлено потоком
     */
    public function getWBImage(string $dir_path, string $file_name): never
    {   
        $this->getImageSourceFile($dir_path, $file_name);
        
        // ! Случай если файла не было - это вынести в отдельную проверку вверх - в отдельную функцию
        if ($this->checkExistanceOfImageFile()) {

            $this->prepareImageDraft();
            $this->calculateImagePasteArea();
            $this->pasteImage();
            $this->sendWBImage();
            
        } else {
            
            $this->prepareImageDraft();
            $this->sendWBImage();
        }
    }
    
    public function checkExistanceOfImageFile(): bool
    {
        if (!empty($this->creating_image_info->source_file)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getImageSourceFile(string $dir_path, string $file_name): void
    {
        $dir = FileHelper::normalizePath(Yii::$app->basePath . Yii::getAlias('@upload/'.$dir_path));
        
        $news_images_files_list = FileHelper::findFiles($dir);

        $found_file = array_filter($news_images_files_list, fn ($v) => strpos($v, "\\".$file_name.".")
        , ARRAY_FILTER_USE_BOTH);
                
        $this->creating_image_info->source_file = $found_file;
    }
    
    public function prepareImageDraft(): void
    {
        $img_dest = imageCreateTrueColor(static::WHITE_BACKGROUND_WIDTH, static::WHITE_BACKGROUND_HEIGHT);
        $white = imagecolorallocate($img_dest, 255, 255, 255);
        imagefilledrectangle($img_dest, 0, 0, static::WHITE_BACKGROUND_WIDTH, static::WHITE_BACKGROUND_HEIGHT, $white);
        
        $this->creating_image_info->image_dest = $img_dest;
    }
    
    public function calculateImagePasteArea(): void
    {
        $info = getimagesize(current($this->creating_image_info->source_file));
        
        $width = $info[0];
        $height = $info[1];
        $this->creating_image_info->source_width = $width;
        $this->creating_image_info->source_height = $height;

        $this->creating_image_info->image_source = imagecreatefromjpeg(current($this->creating_image_info->source_file));

        $coef = $height/$width;   // Коэффициент диспропорции ширины или высоты
        if ($coef > 1.0) {
            $coef = 1.0 / $coef;
        }

        // Получаем длину и ширину итогового блока
        if ($width > $height) {
            $this->creating_image_info->final_width = static::WHITE_BACKGROUND_WIDTH;
            $this->creating_image_info->final_height = static::WHITE_BACKGROUND_HEIGHT * $coef;
        } else {
            $this->creating_image_info->final_width = static::WHITE_BACKGROUND_WIDTH * $coef;
            $this->creating_image_info->final_height = static::WHITE_BACKGROUND_HEIGHT;
        }

        // Получаем итоговые координаты
        if ($width > $height) {
            $this->creating_image_info->final_x = 0;
            $this->creating_image_info->final_y = (static::WHITE_BACKGROUND_HEIGHT * (1.0 - $coef)) / 2.0;
        } else {
            $this->creating_image_info->final_x = (static::WHITE_BACKGROUND_WIDTH * (1.0 - $coef)) / 2.0 ; 
            $this->creating_image_info->final_y = 0;
        }
    }
    
    public function pasteImage(): void
    {        
        imageCopyResampled($this->creating_image_info->image_dest, 
                $this->creating_image_info->image_source, 
                $this->creating_image_info->final_x, $this->creating_image_info->final_y, 
                0, 0, 
                $this->creating_image_info->final_width, $this->creating_image_info->final_height,
                $this->creating_image_info->source_width, $this->creating_image_info->source_height);
    }
    
    public function sendWBImage(): never
    {
        header("Content-Type: image/jpeg");
        imagejpeg($this->creating_image_info->image_dest, NULL, 100);
        exit();
    }
}
