<?php

namespace app\components\image_manager\entities;

use \GDImage;

/**
 * DTO-класс для хранения вспомогательной информации для создания изображений
 *
 * @author Oleg Pyatin
 */
class CreatingImageInfo
{
    public array $source_file;
    
    public GDImage $image_source;
    
    public GDImage $image_dest;
    
    public int $source_width;
    
    public int $source_height;
    
    public int $final_width;
    
    public int $final_height;
    
    public int $final_x;
    
    public int $final_y;
}
