<?php

namespace app\components\enums;

/**
 * Перечисление для характера изменения вывода данных в каталоге
 *
 * @author Oleg Pyatin
 */
enum CatalogUpdateType: string
{
    /**
     * Меняем способ вывода (плиткой, строками)
     */
    case Display = 'tile';
    /**
     * Меняем количество выводимых товаров
     */
    case Pagination = 'pagination';
    /**
     * Меняем параметр сортировки
     */
    case Sort = 'sort';
    /**
     * Меняем фильтрацию для выводимых товаров
     */
    case Filter = 'filter';
    /**
     * Меняем текущую страницу каталога
     */
    case Page = 'page';
    /**
     * Вариант бездействия
     */
    case Cancel = 'cancel';
}
