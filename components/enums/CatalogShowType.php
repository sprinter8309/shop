<?php

namespace app\components\enums;

/**
 * Перечисление для способов вывода каталога
 *
 * @author Oleg Pyatin
 */
enum CatalogShowType: string
{
    /**
     * Выводим плиткой
     */
    case Tile = 'tile';
    /**
     * Выводим строками
     */
    case Line = 'line';
    
    public function stringName(): string
    {
        return match($this) {
            CatalogShowType::Tile => 'tile',
            CatalogShowType::Line => 'line',
        };
    }
}
