<?php

namespace app\components;
use yii\base\Widget;
use app\models\Items;

class ItemsList extends Widget
{
    public function run(): string
    {
        // Берем условные 3 товара из таблицы
        $item_id_array = [5, 8, 14];

        // Получаем по ним выборку нужной информации из БД
        $items = Items::findAll($item_id_array);

        return $this->render('items_list', [
            'items' => $items
        ]);
    }
}
