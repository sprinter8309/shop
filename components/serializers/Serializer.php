<?php

namespace app\components\serializers;

use yii\base\Exception;
use yii\helpers\ArrayHelper;

class Serializer
{
    /**
     * @var array
     */
    private array $properties = [];

    /**
     * Serializer constructor.
     * @param mixed ...$serializtors
     *
     * @throws Exception
     */
    public function __construct(...$serializtors)
    {
        foreach ($serializtors as $serializtor) {
            $this->addProperties($serializtor);
        }
    }

    /**
     * @param mixed ...$serializtors
     *
     * @return Serializer
     * @throws Exception
     */
    public static function create(...$serializtors): Serializer
    {
        return new static($serializtors);
    }

    /**
     * @param string|AbstractProperties $serializtor
     * @return $this
     * @throws Exception
     */
    public function addProperties($serializtor): self
    {
        /** @var AbstractProperties $serializtor */
        $serializtor = $serializtor instanceof AbstractProperties ? $serializtor : new $serializtor();

        foreach ($serializtor->getProperties() as $className => $classDescription) {

            if (is_string($classDescription)) {
                $this->addProperties($classDescription);
                continue;
            }

            if (array_key_exists($className, $this->properties)) {
                if (array_diff(array_keys($this->properties[$className]), array_keys($classDescription))) {
                    throw new Exception("Serializator already have params for {$className}");
                }
                continue;
            }
            $this->properties[$className] = $classDescription;
        }

        return $this;
    }

    /**
     * Serialize object to array
     *
     * @param $object
     * @return array
     */
    public function serialize($object): array
    {
        return ArrayHelper::toArray($object, $this->properties);
    }
}