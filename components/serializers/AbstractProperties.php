<?php

namespace app\components\serializers;

use yii\base\BaseObject;
use yii\base\Exception;

/**
 * Interface SerializatorInterface
 * @package common\components\serizaliators
 *
 * @property array $properties
 */
abstract class AbstractProperties extends BaseObject
{
    /**
     * @return array
     */
    abstract public function getProperties(): array;

    /**
     * Create serializer for current class
     *
     * @return Serializer
     *
     * @throws Exception
     */
    public static function createSerializer(): Serializer
    {
        return new Serializer(static::class);
    }

    /**
     * Serialize use current class
     *
     * @param $object
     *
     * @return array
     *
     * @throws Exception
     */
    public static function serialize($object): array
    {
        return static::createSerializer()->serialize($object);
    }
}