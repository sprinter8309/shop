<?php

namespace app\components;
use yii\base\Widget;

class NewsMain extends Widget
{
    public array $news;

    public function run(): string
    {
        $news = $this->news;

        return $this->render('news_main', [
            'latest_news' => $news
        ]);
    }
}
