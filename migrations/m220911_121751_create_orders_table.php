<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m220911_121751_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_options = null;

        if ($this->db->driverName === 'mysql') {
            $table_options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'products' => $this->text()->notNull(),
            'date_creation' => $this->dateTime(),
            'user_comment' => $this->text(),
            'user_id' => $this->integer(),
            'user_name' => $this->string(32)->notNull(),
            'user_phone' => $this->string(16),
            'order_status' => $this->smallInteger(),
        ], $table_options);
        
        $this->createIndex('idx_orders-user_id', 'orders', 'user_id');
        $this->createIndex('idx_orders-order_status', 'orders', 'order_status');
        
        $this->addForeignKey('fk_orders-user_id', 'orders', 'user_id', 'users', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_orders-user_id', 'orders');
        
        $this->dropIndex('idx_orders-order_status', 'orders');
        $this->dropIndex('idx_orders-user_id', 'orders');
        
        $this->dropTable('{{%orders}}');
    }
}
