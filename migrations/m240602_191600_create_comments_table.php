<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m240602_191600_create_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_options = null;
        
        if ($this->db->driverName === 'mysql') {
            $table_options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%comments}}', [
            'id' => $this->primaryKey(),
            'content' => $this->text(2048),
            'status' => $this->string(128)->notNull(),
            'news_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'create_date' => $this->dateTime(),
            'update_date' => $this->dateTime(),
        ], $table_options);
        
        $this->createIndex('idx_comments-news_id', 'comments', 'news_id');
        $this->createIndex('idx_comments-user_id', 'comments', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_comments-user_id', 'comments');
        $this->dropIndex('idx_comments-news_id', 'comments');
        
        $this->dropTable('{{%comments}');
    }
}
