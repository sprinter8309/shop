<?php

use yii\db\Migration;

/**
 * Handles the creation of table `items`.
 */
class m220911_121735_create_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_options = null;

        if ($this->db->driverName === 'mysql') {
            $table_options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%items}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(192)->notNull(),
            'index_code' => $this->integer(),
            'price' => $this->float(),
            'image' => $this->string(256),
            'short_description' => $this->text(),
            'full_description' => $this->text(),
            'availability' => $this->boolean()->notNull(),
            'is_new' => $this->boolean()->notNull(),
            'is_recommended' => $this->boolean()->notNull(),
            'is_hit' => $this->boolean()->notNull(),
            'category_id' => $this->integer(),
            'producer_id' => $this->integer(),
            'rating' => $this->float(),
            'status' => $this->tinyInteger(),
        ], $table_options);
        
        $this->createIndex('idx_items-name', 'items', 'name');
        $this->createIndex('idx_items-index_code', 'items', 'index_code');
        $this->createIndex('idx_items-category_id', 'items', 'category_id');
        $this->createIndex('idx_items-producer_id', 'items', 'producer_id');
        $this->createIndex('idx_items-rating', 'items', 'rating');
        $this->createIndex('idx_items-status', 'items', 'status');
        
        $this->addForeignKey('fk_items-category_id', 'items', 'category_id', 'category', 'id');
        $this->addForeignKey('fk_items-producer_id', 'items', 'producer_id', 'producers', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_items-producer_id', 'items');
        $this->dropForeignKey('fk_items-category_id', 'items');
        
        $this->dropIndex('idx_items-status', 'items');
        $this->dropIndex('idx_items-rating', 'items');
        $this->dropIndex('idx_items-producer_id', 'items');
        $this->dropIndex('idx_items-category_id', 'items');
        $this->dropIndex('idx_items-index_code', 'items');
        $this->dropIndex('idx_items-name', 'items');
        
        $this->dropTable('{{%items}}');
    }
}
