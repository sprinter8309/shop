<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m220911_121528_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_options = null;

        if ($this->db->driverName === 'mysql') {
            $table_options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'url_name' => $this->string(64)->notNull(),
            'image' => $this->string(192),
            'level_cat' => $this->tinyInteger()->notNull(),
            'parent_cat' => $this->integer(),
            'sort_num' => $this->integer()->notNull(),
        ], $table_options);
        
        $this->createIndex('idx_category-url_name', 'category', 'url_name');
        $this->createIndex('idx_category-parent_cat', 'category', 'parent_cat');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_category-parent_cat', 'category');
        $this->dropIndex('idx_category-url_name', 'category');
        
        $this->dropTable('{{%category}}');
    }
}
