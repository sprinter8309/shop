<?php

use yii\db\Migration;

/**
 * Handles the creation of table `producers`.
 */
class m220911_121620_create_producers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_options = null;

        if ($this->db->driverName === 'mysql') {
            $table_options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%producers}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(48)->notNull()->unique(),
        ], $table_options);
        
        $this->createIndex('idx_producers-name', 'producers', 'name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_producers-name', 'producers');
        
        $this->dropTable('{{%producers}}');
    }
}
