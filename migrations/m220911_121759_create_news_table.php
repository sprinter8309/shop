<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m220911_121759_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_options = null;

        if ($this->db->driverName === 'mysql') {
            $table_options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'preview_text' => $this->text(512),
            'detail_text' => $this->text(8192)->notNull(),
            'image' => $this->string(256)->notNull(),
            'creation_date' => $this->date()
        ], $table_options);
        
        $this->createIndex('idx_news-name', 'news', 'name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_news-name', 'news');
        
        $this->dropTable('{{%news}}');
    }
}
