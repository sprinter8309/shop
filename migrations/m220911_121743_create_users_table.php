<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m220911_121743_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_options = null;

        if ($this->db->driverName === 'mysql') {
            $table_options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string(32)->notNull()->unique(),
            'email' => $this->string(96)->notNull()->unique(),
            'password' => $this->string(32)->notNull(),
            'phone' => $this->string(16)->notNull(),
            'last_visit' => $this->dateTime(),
            'date_creation' => $this->dateTime(),
            'status' => $this->tinyInteger(),
        ], $table_options);
        
        $this->createIndex('idx_users-login', 'users', 'login');
        $this->createIndex('idx_users-email', 'users', 'email');
        $this->createIndex('idx_users-status', 'users', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_users-status', 'users');
        $this->dropIndex('idx_users-email', 'users');
        $this->dropIndex('idx_users-login', 'users');
        
        $this->dropTable('{{%users}}');
    }
}
