<?php
$this->title = 'Заказы на покупки';

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\BaseLibrary;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = [
	'template' => '<li>{link}</li> ',
	'label' => 'Личный кабинет',
	'url' => [ Url::to(['/cabinet'])]
];

$this->params['breadcrumbs'][] = [
	'template' => '<span class="breadcrumbs-divide">&gt;</span> <li>{link}</li>',
	'label' => 'Ваши заказы'
];
?>

<div>
    <?php
        echo Breadcrumbs::widget([
            'options' => [
                    'class' => 'breadcrumbs-path'
            ],
            'homeLink'=> false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [ ]
        ]);
    ?>
</div>

<h2>Ваши заказы</h2>

<?php if (count($user_orders)): ?>
    <div class="cabinet-order-container">
        <?php foreach($user_orders as $order): ?>
            <div class="cabinet-order-item-point">

                <div class="cabinet-order-item-id  cabinet-order-light-item-fon">
                        <?= $order['id'] ?>
                </div>
                <div class="cabinet-order-item-date  cabinet-order-dark-item-fon">
                    <?php
                        $date_array = date_parse($order['date_creation']);
                        echo $date_array['day'] . '.' . $date_array['month'] . '.' .  $date_array['year'];
                    ?>
                </div>
                <div class="cabinet-order-item-products  cabinet-order-light-item-fon">

                    <?php foreach($orders_info[ $order['id'] ]['items'] as $item): ?>
                        <div class="cabinet-order-item-products-point">
                            <div class="cabinet-order-item-point-image">
                                <div class="circle-width-container circle-width-cart-container">
                                    <?= Html::img('/site/wb-image/'.(explode('.', $item["image"])[0])) ?>
                                </div>
                            </div>
                            <div class="cabinet-order-item-point-name">
                                <?= $item["name"] . ' (' . $orders_info[ $order['id'] ]['products_cart'][ $item["id"] ] . 'шт)' ?>
                            </div>
                        </div>
                        <div class="cabinet-order-products-item-offset"></div>
                    <?php endforeach; ?>
                </div>
                <div class="cabinet-order-item-price  cabinet-order-dark-item-fon">
                        <?= $orders_info[ $order['id'] ]['total_price'] . '.00 руб.' ?>
                </div>
                <div class="cabinet-order-item-status  cabinet-order-light-item-fon">
                        <?= BaseLibrary::getStatusText( $order['order_status'] ) ?>
                </div>
            </div>
            <div class="cabinet-order-item-offset"></div>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <div class="cabinet-no-orders">
            У вас нет заказов
    </div>
<?php endif; ?>

<div class="cabinet-order-return">
    <?= Html::a( 'Вернуться назад', Url::to(['/cabinet']) ); ?>
</div>
