<?php
$this->title = 'Ваш кабинет';
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h2>Учетная запись пользователя <?= $user["login"] ?></h2>

<div class="cabinet-menu-container">
    <div class="cabinet-menu-point">
            <?= Html::a( 'Редактировать данные', Url::to(['/cabinet/update']) ); ?>
    </div>

    <div class="cabinet-menu-point">
            <?= Html::a( 'Смотреть историю заказов', Url::to(['/cabinet/orders']) ); ?>
    </div>
</div>



