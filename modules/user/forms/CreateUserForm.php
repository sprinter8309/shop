<?php

namespace app\modules\user\forms;

use yii\base\Model;
use app\components\validators\PhoneValidator;
use app\models\User;

/**
 * Форма для выполнения входа в систему
 * 
 * @author Oleg Pyatin
 */
class CreateUserForm extends Model
{
    public function __construct(public string $login = '',
                                public string $password = '',
                                public string $phone = '',
                                public string $email = '')
    {

    }
    
    public function rules(): array
    {
        return [
            ['login', 'required', 'message' => 'Не введено имя'],
            ['password', 'required', 'message' => 'Не введен пароль'],
            ['email', 'required', 'message' => 'Не введен email'],
            ['email', 'email', 'message' => 'Неправильный email'],
            ['password', 'string', 'min' => 8, 'message' => 'Пароль минимум от 8 символов'],
            ['phone', PhoneValidator::class],
            [['login'], 'unique', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['login' => 'login']],
            [['email'], 'unique', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['email' => 'email']],
        ];
    }
}
