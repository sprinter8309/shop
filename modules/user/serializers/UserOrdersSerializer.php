<?php

namespace app\modules\user\serializers;

use app\models\Orders;
//use app\modules\user\entities\UserOrdersData;
use app\components\serializers\AbstractProperties;

class UserOrdersSerializer extends AbstractProperties
{
    public function getProperties(): array 
    {
        return [
            Orders::class=>[
                'id',
                'products',
                'date_creation',
                'user_comment',
                'user_name',
                'user_phone',
                'order_status',
            ]
        ];
    }
}
