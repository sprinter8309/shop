<?php

namespace app\modules\user\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\components\constants\NavigationConst;
use app\modules\user\services\UserService;
use app\modules\user\forms\LoginUserForm;
use app\modules\user\forms\CreateUserForm;
use app\modules\user\serializers\UserSerializer;
use app\modules\user\serializers\UserOrdersSerializer;

/**
 * Default controller for the `user` module
 */
class BaseController extends Controller
{
    public function __construct($id, $module,
            protected UserService $user_service,
            $config = [])
    {
        $this->user_service = $user_service;
        
        parent::__construct($id, $module, $config);
    }
    
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['cabinet', 'update', 'orders', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['cabinet', 'update', 'orders', 'logout'],
                        'roles' => ['@'],
                    ]
                ],
                'denyCallback'=>function ($rule, $action) {
                    return Yii::$app->response->redirect([NavigationConst::URL_LOGIN]);
                }
            ]
        ];
    }
    
    public function actionLogin(): string
    {
        if (Yii::$app->request->post()) {
            $login_user_form = $this->user_service->loginUserActions(Yii::$app->request->bodyParams);
        } else {
            $login_user_form = Yii::createObject(LoginUserForm::class);
        }
        
        return $this->render('login', [
            'model' => $login_user_form,
        ]);
    }
    
    public function actionRegister(): string
    {
        if (Yii::$app->request->post()) {
            $create_user_form = $this->user_service->registerUser(Yii::$app->request->bodyParams);
        } else {
            $create_user_form = Yii::createObject(CreateUserForm::class);
        }
        
        return $this->render('register', [
            'model' => $create_user_form,
        ]);
    }
    
    public function actionUpdate(): string
    {
        if (Yii::$app->request->post()) {
            $update_user_form = $this->user_service->updateUser(Yii::$app->request->bodyParams);
        } else {
            $update_user_form = $this->user_service->getUserDataForUpdate();
        }
        
        return $this->render('update', [
            'model' => $update_user_form,
        ]);
    }
    
    public function actionCabinet(): string
    {
        return $this->render('cabinet', [ 
            'user'=>UserSerializer::serialize($this->user_service->getCurrentUser())
        ]);
    }
    
    public function actionOrders(): string
    {
        return $this->render('orders', UserOrdersSerializer::serialize($this->user_service->getUserOrdersData()));
    }
     
    public function actionLogout(): void
    {
        $this->user_service->logout();
    }   
}
