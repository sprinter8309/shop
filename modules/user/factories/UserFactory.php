<?php

namespace app\modules\user\factories;

use app\helpers\DateHelper;
use app\components\constants\UserConst;
use app\models\User;
use app\modules\user\forms\CreateUserForm;

/**
 * Класс фабрика для создания записей о новых пользователях
 * 
 * @author Oleg Pyatin
 */
class UserFactory
{
    public function createUser(CreateUserForm $create_user_form, int $new_user_id): User
    {
        $new_user = new User;
        
        $new_user->id = $new_user_id;
        $new_user->login = $create_user_form->login;
        $new_user->email = $create_user_form->email;
        $new_user->setPassword($create_user_form->password);
        $new_user->phone = $create_user_form->phone;
        $new_user->last_visit = DateHelper::now();
        $new_user->date_creation = DateHelper::now();
        $new_user->status = UserConst::USER_STATUS_ACTIVE;
        
        return $new_user;
    }
}
