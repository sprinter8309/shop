<?php
$this->title = 'Главная страница';
use app\components\ItemsList;
use app\components\NewsMain;
use app\components\ItemDay;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="main-first-row">
    <div class="main-popular-items-name">
            Хиты продаж
    </div>
    <?= ItemsList::widget() ?>
    <div class="main-popular-items-link">
            <?= Html::a( 'Перейти в каталог', Url::to(['/catalog']), ['class'=>'main-popular-link-button']); ?>
    </div>
</div>

<div class="main-second-row clearfix">
    <div class="main-item-day-block">
        <div class="main-widget-name">
                Товар дня
        </div>
        <?= ItemDay::widget() ?>
    </div>
    <div class="main-news-block">
        <div class="main-widget-name">
                Последние новости
        </div>
        <?= NewsMain::widget(['news'=>$news]) ?>
    </div>
</div>
