<?php
$this->title = 'Сведения об оплате';
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

// Добавление хлебных крошек

// Верхний уровень хлебных крошек
$this->params['breadcrumbs'][] = [
    'template' => '<li>{link}</li> ',
    'label' => 'Контакты',
    'url' => [ Url::to(['/contact'])]
];

$this->params['breadcrumbs'][] = [
    'template' => '<span class="breadcrumbs-divide">&gt;</span> <li>{link}</li>',
    'label' => 'Сведения об оплате'
];

?>

<div class="contact-common-container clearfix">
    <div class="contact-left-block">
        <?php
            echo Menu::widget([
                'items' => [
                    ['label'=>'Контактные данные', 'url'=>['/contact']],
                    ['label'=>'Пользовательское соглашение', 'url'=>['/agreement']],
                    ['label'=>'Политика конфиденциальности', 'url'=>['/policy']],
                    ['label'=>'Сведения об оплате', 'url'=>['/pay-info']],
                ],
                'options' =>[
                    'class'=>'contact-menu'
                ]
            ]);
        ?>
    </div>
    <div class="contact-right-block">
        <div>
            <?php
                echo Breadcrumbs::widget([
                    'options' => [
                            'class' => 'breadcrumbs-path'
                    ],
                    'homeLink'=> false,
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [ ]
                ]);
            ?>
        </div>
        <div class="contact-head-name">
                Сведения об оплате
        </div>
        <div class="contact-text">
            <div class="contact-paragraph-name">
                    Способы оплаты
            </div>
            <div class="contact-paragraph">
                <ul class="contact-paragraph-list">
                    <li>
                        <span>Наличный расчет</span>
                        Наличными можно оплатить покупку в любом отделении магазина или курьеру (при доставке товара)
                    </li>
                    <li>
                        <span>Пластиковые карты</span>
                        Использование безналичного расчета картой одного из банка в магазине или при доставке
                        (принимаются карты платежных систем - VISA, MasterCard, Maestro, Paypal)
                    </li>
                    <li>
                        <span>Online-оплата</span>
                        Использование безналичного расчета картой одного из банка в магазине или при доставке
                        (принимаются карты платежных систем - VISA, MasterCard, Maestro, Paypal)
                        <br/><br/>
                        В этом способе используется пошаговая схема, где вы вводите информацию в защищенной форме оплаты на странице компании-партнера,
                            далее производится авторизация платежа и подтверждение заказа
                        <br/><br/>
                        На номер телефона клиента дополнительно после выполнения оплаты присылается специальный SMS с кодом, который используется для верификации платежа
                        <br/><br/>
                        * При возврате платежа деньги вернутся на то платежное средство (банковскую карту, QIWI, WebMoney, Яндекс.деньги с
                            которого был произведен платеж)
                    </li>
                    <li>
                        <span>Кредит</span>
                        Оформление заявки через банки-партнеры нашего магазины. Дополнительную информацию вы сможете получить в отделениях магазина
                    </li>
                </ul>
            </div>

            <div class="contact-paragraph-name">
                    Безопасность платежей
            </div>
            <div class="contact-paragraph">
                    Предоставляемая информация (фамилия, имя, телефон, e-mail, номер банковской карты), являются конфидециальными и
                        обеспечены защитой от несанкционированного доступа
            </div>
            <div class="contact-paragraph-name">
                    Возврат товара
            </div>
            <div class="contact-paragraph">
                    В соотвествии с Законом о защите прав потребителей, может производится обмен и возврат товара
                    <br><br>
                    Возврат денежных средств производится или наличным или безналичным расчетом
                    <br><br>
                    В случае наличного расчета требуется подойти в отделение магазина, показать документы удостоверяющие личность (паспорт)
                        и заполнить бланк заявки
            </div>
        </div>

    </div>
</div>