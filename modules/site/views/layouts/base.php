<?php

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\helpers\Url;
use app\helpers\CartHelper;

AppAsset::register($this);
$this->beginPage(); 
?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= Html::csrfMetaTags() ?>
        <meta name="theme-color" content="#fff">
        <meta name="description" content="Интернет-магазин SHOP">
        <meta name="keywords" content="">
        <meta http-equiv="Content-Language" content="ru">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta charset="<?= Yii::$app->charset ?>"> 

        <?php $this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']) ?>

        <title><?= Yii::$app->name ?></title>

        <style>
            .svg-city-symbol {
                width: 23px;
                height: 30px;
                fill: transparent;
                stroke: currentColor;
                stroke-width: 2px;
            }
            .svg-city-pointer {
                padding-left: 1px;
                width: 15px;
                height: 30px;
                fill: currentColor;
            }

            .svg-cart-symbol {
                width: 30px;
                height: 30px;
                stroke: currentColor;
                stroke-width: 2px;
                fill: currentColor;
            }

            .svg-cook-menu-symbol, .svg-home-menu-symbol, .svg-tv-menu-symbol, .svg-office-menu-symbol {
                width: 50px;
                height: 46px;
                fill: transparent;
                stroke: currentColor;
                stroke-width: 2px;
            }

            .svg-phone-symbol, .svg-auth-symbol, .svg-reg-symbol, .svg-input-symbol, .svg-account-symbol, .svg-output-symbol {
                width: 22px;
                height: 18px;
                stroke: currentColor;
                stroke-width: 3px;
                fill: transparent;
                vertical-align: bottom;
            }

            .svg-account-symbol {
                fill: currentColor;
                margin-right: 4px;
            }

            .svg-output-symbol {
                margin-left: 8px;
            }

            .svg-reg-symbol {
                width: 28px;
                margin-left: 7px;
            }

            .svg-input-symbol {
                margin-right: 4px;
            }

            .thin-line {
                stroke-width: 2px;
            }

            .red {
                color: red;
            }

            .menu-test {
                cursor: pointer;
                color: #5a5a5a;
                transition: color 0.15s;
                padding-top: 10px;
            }

            .svg-home-menu-symbol path {
                stroke-width: 1px;
            }

            .svg-tv-menu-symbol line[light-gray] {
                stroke-width: 1px;
                color: #9a9a9a;
            }

            .svg-office-menu-symbol rect[gray] {
                fill: currentColor;
            }

            .menu-test span {
                vertical-align: top;
                padding-top: 5px;
                padding-left: 5px;
                font-size: 17px;
                display: inline-block;
                max-width: 160px;
            }
        </style>

        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="page">
            <div class="center">
                <header>
                    <div class="high-panel">
                        <div class="main-panel clearfix">
                            <div class="city-menu">
                                <svg class="svg-city-symbol">
                                    <circle r="3" cx="10" cy="15" ></circle>
                                    <path d="M3 20   Q 0 8    10 7"/>
                                    <path d="M10 7   Q 20 8   17 20"/>

                                    <path d="M3 20   Q 6 26   10 27"/>
                                    <path d="M10 27   Q 14 26   17 20"/>
                                </svg>
                                <span class="city-menu-label">
                                    Киров
                                </span>
                                <svg class="svg-city-pointer">
                                    <polygon points="0,15 8,15 4,21"></polygon>
                                </svg>
                            </div>
                            <div class="high-info-block">
                                <?= Html::a('Доставка', Url::to('/delivery'), ['class'=>'high-info-block-link']); ?>
                                <?= Html::a('Оплата', Url::to('/pay-info'), ['class'=>'high-info-block-link']); ?>
                                <?= Html::a('Адреса', Url::to('/shop-points'), ['class'=>'high-info-block-link']); ?>
                            </div>
                            <div class="high-auth-block">
                                <span>
                                    <?php if (Yii::$app->user->isGuest): ?>
                                        <?= Html::a('<svg class="svg-input-symbol">
                                                        <line class="thin-line" x1="5" y1="1" x2="15" y2="1"/>
                                                        <line class="thin-line" x1="15" y1="1" x2="15" y2="17"/>
                                                        <line class="thin-line" x1="5" y1="17" x2="15" y2="17"/>

                                                        <line class="thin-line" x1="1" y1="9" x2="9" y2="9"/>
                                                        <polygon points="7,8 9,9 7,10"/>
                                                    </svg>'
                                                . 'Вход', Url::to('/login'), ['class'=>'high-info-block-link high-auth-block-link']); ?>
                                    <?php else: ?>
                                        <?php $account_name = Yii::$app->user->identity->login ?? 'Аккаунт';
                                                echo Html::a('<svg class="svg-account-symbol">
                                                        <circle class="thin-line fill-account" r="3" cx="10" cy="4" ></circle>
                                                        <path class="thin-line" d="M3 17 V 14  Q 3 10  10 10  Q 17 10 17 14 V 17 Z"/>
                                                    </svg>'.$account_name, Url::to('/cabinet'), ['class'=>'high-info-block-link high-auth-block-link']); ?>
                                    <?php endif; ?>
                                </span>
                                <span>
                                    <?php if (Yii::$app->user->isGuest): ?>
                                        <?= Html::a('Регистрация<svg class="svg-reg-symbol">
                                                        <circle class="thin-line" r="3" cx="5" cy="13" ></circle>
                                                        <circle class="thin-line" r="4" cx="6" cy="12" ></circle>
                                                        <line class="thin-line" x1="6" y1="9" x2="11" y2="12"/>
                                                        <line class="" x1="8" y1="10" x2="17" y2="2"/>
                                                        <line class="thin-line" x1="16" y1="3" x2="19" y2="5"/>
                                                        <line class="thin-line" x1="14" y1="5" x2="16" y2="8"/>
                                                    </svg>', Url::to('/register'), ['class'=>'high-info-block-link high-auth-block-link']); ?>
                                    <?php else: ?>
                                        <?= Html::a('Выход<svg class="svg-output-symbol">
                                                        <line class="thin-line" x1="1" y1="1" x2="11" y2="1"/>
                                                        <line class="thin-line" x1="1" y1="1" x2="1" y2="17"/>
                                                        <line class="thin-line" x1="1" y1="17" x2="11" y2="17"/>

                                                        <line class="thin-line" x1="7" y1="9" x2="15" y2="9"/>
                                                        <polygon points="9,8 7,9 9,10"/>
                                                    </svg>', Url::to('/logout'), ['class'=>'high-info-block-link high-auth-block-link']); ?>
                                    <?php endif; ?>
                                </span>
                            </div>
                            <div class="high-phone-block">

                                <?= Html::a(''
                                        . ' <svg class="svg-phone-symbol">
                                                <path d="M5 3   Q 5 13    14 14"/>
                                                <line class="thin-line" x1="5" y1="3" x2="8" y2="5"/>
                                                <line class="thin-line" x1="13" y1="12" x2="15" y2="14"/>
                                            </svg>'
                                        . '8 (800) 750-90-30', Url::to('tel://88007509030'), ['class'=>'high-info-block-link']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="main-panel clearfix">
                        <div class="logo-block">
                            <?php echo Html::a(Html::img('/web/upload/site_images/logo.png'), Url::to('/')); ?>
                        </div>
                        <div class="cart-block">
                            <svg class="svg-cart-symbol">
                                <line x1="4" y1="8" x2="8" y2="8"/>
                                <line x1="7" y1="8" x2="9" y2="11"/>
                                <line x1="8" y1="11" x2="26" y2="11"/>

                                <line x1="9" y1="11" x2="11" y2="20"/>
                                <line x1="25" y1="11" x2="23" y2="20"/>
                                <line x1="11" y1="19" x2="23" y2="19"/>

                                <!--<line x1="12" y1="22" x2="12" y2="24"/>-->
                                <circle r="1" cx="11" cy="23" ></circle>
                                <circle r="1" cx="23" cy="23" ></circle>
                            </svg>
                            <a href="/cart">Корзина (<span class="head-cart-quant"><?= CartHelper::countCartItems() ?></span>)</a>
                        </div>
                    </div>
                </header>
                <main>
                    <section class="menu-panel">
                        <div class="main-panel clearfix">
                            <?php
                                echo Menu::widget([
                                    'items' => [
                                        ['label'=>'Главная', 'url'=>['/']],
                                        ['label'=>'Каталог', 'url'=>['/catalog']],
                                        ['label'=>'Новости', 'url'=>['/news']],
                                        ['label'=>'Контакты', 'url'=>['/contact']],
                                    ],
                                    'options' =>[
                                        'class'=>'high-menu'
                                    ]
                                ]);
                            ?>
                        </div>
                    </section>
                    <div class="main-panel content-field clearfix">
                        <?php if ( strpos(Yii::$app->request->url, '/catalog') !== false ): ?>
                            <section class="left-block">
                                <?php

                                    $link_cook_item = '<svg class="svg-cook-menu-symbol">
                                                            <line x1="5" y1="10" x2="8" y2="4"/>
                                                            <line x1="8" y1="4" x2="33" y2="4"/>
                                                            <line x1="33" y1="4" x2="36" y2="10"/>

                                                            <line x1="11" y1="7" x2="19" y2="7"/>
                                                            <line x1="23" y1="7" x2="31" y2="7"/>

                                                            <line x1="9" y1="13" x2="12" y2="13"/>
                                                            <line x1="20" y1="13" x2="23" y2="13"/>
                                                            <line x1="25" y1="13" x2="28" y2="13"/>
                                                            <line x1="30" y1="13" x2="33" y2="13"/>

                                                            <rect x="5" y="10" width="31" height="25" rx="2"></rect>
                                                            <rect x="5" y="16" width="31" height="14"></rect>
                                                        </svg><span>Кухонная техника</span>';

                                    $link_home_item = '<svg class="svg-home-menu-symbol">
                                                            <line x1="5" y1="16" x2="36" y2="16"/>
                                                            <circle r="8" cx="21" cy="28" ></circle>

                                                            <path d="M22 24   Q 26 24    26 29"/>

                                                            <line x1="9" y1="13" x2="12" y2="13"/>

                                                            <line x1="20" y1="13" x2="23" y2="13"/>
                                                            <line x1="25" y1="13" x2="28" y2="13"/>
                                                            <line x1="30" y1="13" x2="33" y2="13"/>

                                                            <rect x="5" y="10" width="31" height="32" rx="2"></rect>
                                                        </svg><span>Техника для дома</span>';

                                    $link_tv_item = '<svg class="svg-tv-menu-symbol">
                                                            <line x1="18" y1="36" x2="18" y2="40"/>
                                                            <line x1="24" y1="36" x2="24" y2="40"/>
                                                            <line x1="12" y1="40" x2="30" y2="40"/>

                                                            <line light-gray x1="26" y1="31" x2="35" y2="22"/>
                                                            <line light-gray x1="30" y1="31" x2="35" y2="26"/>

                                                            <!--<line x1="5" y1="12" x2="40" y2="12"/>-->
                                                            <line x1="3" y1="34" x2="38" y2="34"/>
                                                            <rect x="3" y="10" width="35" height="26" rx="2"></rect>
                                                        </svg><span>Телевизоры, <br>аудио, видео</span>';

                                    $link_office_item = '<svg class="svg-office-menu-symbol">
                                                            <rect x="9" y="6" width="24" height="4" rx="1"></rect>

                                                            <rect x="5" y="10" width="32" height="26" rx="2"></rect>
                                                            <rect gray x="11" y="16" width="20" height="2" rx="1"></rect>

                                                            <line thin x1="11" y1="10" x2="11" y2="32"/>
                                                            <line thin x1="31" y1="10" x2="31" y2="32"/>
                                                            <line x1="34" y1="13" x2="34" y2="15"/>
                                                            <line x1="34" y1="18" x2="34" y2="20"/>

                                                            <line thin x1="5" y1="32" x2="37" y2="32"/>
                                                        </svg><span>Офисная техника</span>';

                                    $left_full_menu = Menu::widget([
                                        'items'=>[
                                            [
                                                'label'=>$link_cook_item,
                                                'url'=>['/catalog/cookers'],
                                                'items' => [
                                                    ['label'=>'Кухонные плиты', 'url'=>['/catalog/cookers'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'Газовые плиты', 'url'=>['/catalog/gas-cookers'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Комбинированные плиты', 'url'=>['/catalog/combined-cookers'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Настольные плиты', 'url'=>['/catalog/table-cookers'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Электрические плиты', 'url'=>['/catalog/electric-cookers'], 'options'=>['class'=>'left-full-nested-link']],

                                                    ['label'=>'Холодильники', 'url'=>['/catalog/refrigerators'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'Однокамерные холодильники', 'url'=>['/catalog/single-compartment-refrigerators'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Двухкамерные холодильники', 'url'=>['/catalog/two-chamber-refrigerators'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Трехкамерные холодильники', 'url'=>['/catalog/three-compartment-refrigerators'], 'options'=>['class'=>'left-full-nested-link']],

                                                    ['label'=>'Вытяжки', 'url'=>['/catalog/hoods'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'Встраиваемые вытяжки', 'url'=>['/catalog/build_in_hoods'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Каминные вытяжки', 'url'=>['/catalog/chimney_hoods'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Подвесные вытяжки', 'url'=>['/catalog/suspension_hoods'], 'options'=>['class'=>'left-full-nested-link']],
                                                ],
                                            ],
                                            [
                                                'label'=>$link_home_item,
                                                'url'=>['/catalog/washing-machines'],
                                                'items' => [
                                                    ['label'=>'Стиральные машины', 'url'=>['/catalog/washing-machines'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'Автоматические стиральные машины', 'url'=>['/catalog/automatic-washing-machines'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Полуавтоматические стиральные машины', 'url'=>['/catalog/semi-automatic-washing-machines'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Ультразвуковые стиральные машины', 'url'=>['/catalog/ultrasonic-washing-machines'], 'options'=>['class'=>'left-full-nested-link']],

                                                    ['label'=>'Пылесосы', 'url'=>['/catalog/vacuum-cleaners'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'Моющие пылесосы', 'url'=>['/catalog/washing-vacuum-cleaners'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Пылесосы для сухой уборки', 'url'=>['/catalog/dry-cleaning-vacuum-cleaners'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Пылесосы с аквафильтром', 'url'=>['/catalog/vacuum-cleaners-aquafilter'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Пылесосы с турбощеткой', 'url'=>['/catalog/vacuum-cleaners-turbo-brash'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Роботы-пылесосы', 'url'=>['/catalog/robots-vacuum-cleaners'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Ручные пылесосы', 'url'=>['/catalog/handheld-vacuum-cleaners'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Циклонные пылесосы', 'url'=>['/catalog/cyclone-vacuum-cleaners'], 'options'=>['class'=>'left-full-nested-link']],

                                                    ['label'=>'Обогреватели', 'url'=>['/catalog/heaters'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'Инфракрасные обогреватели', 'url'=>['/catalog/infrared-heaters'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Конвекторы', 'url'=>['/catalog/convectors'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Масляные радиаторы', 'url'=>['/catalog/oil-coolers'], 'options'=>['class'=>'left-full-nested-link']],
                                                ]
                                            ],
                                            [
                                                'label'=>$link_tv_item,
                                                'url'=>['/catalog/tv'],
                                                'items' => [
                                                    ['label'=>'Телевизоры', 'url'=>['/catalog/tv'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'4K Телевизоры', 'url'=>['/catalog/4k_tv'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'OLED Телевизоры', 'url'=>['/catalog/oled_tv'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'SMART TV', 'url'=>['/catalog/smart_tv'], 'options'=>['class'=>'left-full-nested-link']],

                                                    ['label'=>'Видео', 'url'=>['/catalog/video'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'DVD Плееры', 'url'=>['/catalog/dvd_players'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Медиаплееры', 'url'=>['/catalog/mediaplayers'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Проекторы', 'url'=>['/catalog/projectors'], 'options'=>['class'=>'left-full-nested-link']],

                                                    ['label'=>'Аудио', 'url'=>['/catalog/audio'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'MP3 Плееры', 'url'=>['/catalog/mp3_players'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Музыкальные центры', 'url'=>['/catalog/music_centers'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Радиоприемники', 'url'=>['/catalog/radios'], 'options'=>['class'=>'left-full-nested-link']],
                                                ]
                                            ],
                                            [
                                                'label'=>$link_office_item,
                                                'url'=>['/catalog/printers'],
                                                'items' => [
                                                    ['label'=>'Принтеры', 'url'=>['/catalog/printers'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'Лазерные принтеры', 'url'=>['/catalog/laser_printers'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Струйные принтеры', 'url'=>['/catalog/inkjet_printers'], 'options'=>['class'=>'left-full-nested-link']],

                                                    ['label'=>'Сканеры', 'url'=>['/catalog/scanners'], 'options'=>['class'=>'left-full-nested-title-link']],

                                                    ['label'=>'Планшетные сканеры', 'url'=>['/catalog/flatbed_scanners'], 'options'=>['class'=>'left-full-nested-link']],
                                                    ['label'=>'Протяжные сканеры', 'url'=>['/catalog/broaching_scanners'], 'options'=>['class'=>'left-full-nested-link']]
                                                ]
                                            ],
                                        ],
                                        'options' =>[
                                            'class'=>'left-full-menu'
                                        ]
                                    ]);

                                    echo htmlspecialchars_decode($left_full_menu);
                                ?>

                                <div class="catalog-filter">
                                    <div class="catalog-filter-item">
                                        <?php if (!empty($this->params['producers_list'])): ?>
                                            <h4>Производитель</h4>
                                            <select class="catalog-filter-choice">
                                            <?php foreach($this->params['producers_list'] as $producer): ?>
                                                <option><?= $producer['name'] ?></option>
                                            <?php endforeach; ?>
                                            </select>
                                        <?php endif; ?>
                                    </div>
                                    <div class="catalog-filter-buttons">
                                        <button class="catalog-filter-button-run">Фильтровать</button>
                                    </div>
                                    <div class="catalog-filter-discard-buttons">

                                    </div>
                                </div>
                            </section>
                        <?php endif; ?>
                        <section class="right-block <?php if ( strpos(Yii::$app->request->url, '/catalog') === false ) echo 'full-right-block'; ?>">
                            <?= $content ?>
                        </section>
                    </div>
                </main>
            </div>
            <footer>
                <div class="main-panel clearfix">
                    <div class="footer-info">
                        &copy; 2018 Интернет-магазин SHOP
                        <p>Бытовая техника, комплектующие, аксессуары</p>
                    </div>
                    <div class="footer-paysistems">
                        Мы принимаем к оплате
                        <p><?php echo Html::img('/web/upload/site_images/1.png'); ?>
                           <?php echo Html::img('/web/upload/site_images/2.png'); ?>
                           <?php echo Html::img('/web/upload/site_images/3.png'); ?>
                           <?php echo Html::img('/web/upload/site_images/4.png'); ?></p>
                    </div>
                    <div class="footer-menu">
                        <?php
                            echo Menu::widget([
                                'items' => [
                                    ['label'=>'Пользовательское соглашение', 'url'=>['/agreement']],
                                    ['label'=>'Политика конфиденциальности', 'url'=>['/policy']],
                                    ['label'=>'Сведения об оплате', 'url'=>['/pay-info']],
                                    ['label'=>'Контакты', 'url'=>['/contact']],
                                ],
                                'options' =>[ ]
                            ]);
                        ?>
                    </div>
                </div>
            </footer>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>

<?php
$this->endPage();
?>
