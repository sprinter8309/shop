<?php

namespace app\modules\cart\controllers;

use yii\web\Controller;
use app\modules\cart\services\CartService;
use app\modules\cart\serializers\CartInfoSerializer;

/**
 * Default controller for the `cart` module
 */
class BaseController extends Controller
{
    public function __construct($id, $module,
            protected CartService $cart_service,
            $config = [])
    {
        $this->cart_service = $cart_service;
        
        parent::__construct($id, $module, $config);
    }
    
    /**
     * Действие вывода окна корзины
     *
     * @return string 
     */
    public function actionCart(): string
    {
        return $this->render('cart', CartInfoSerializer::serialize($this->cart_service->getCartInfo()));
    }
    
    /**
     * Действие добавления товара в корзину
     *
     * @return string 
     */
    public function actionAdd($id): int
    {
        return $this->cart_service->addItemIntoCart($id);
    }
    
    /**
     * Действие удаления товара из корзины
     *
     * @return string 
     */
    public function actionDelete($id): int
    {
        return $this->cart_service->deleteItemFromCart($id);
    }
}
