<?php

namespace app\modules\cart\repositories;

use app\models\Items;

/**
 * Класс репозиторий для данных корзины
 * 
 * @author Oleg Pyatin
 */
class CartRepository
{
    /**
     * Получение товаров (их характеристик) для корзины
     *
     * @param array $products_ids  Список ID товаров
     * @return array  Массив объектов товаров
     */
    public function getItemsForCart(array $products_ids): array
    {
        return Items::findAll($products_ids);
    }
}
