<?php

namespace app\modules\cart\services;

use Yii;
use app\modules\cart\entities\CartInfo;
use app\modules\cart\repositories\CartRepository;

/**
 * Сервис работы с корзиной
 * 
 * @author Oleg Pyatin
 */
class CartService
{
    public function __construct(protected CartRepository $cart_repository)
    {
        $this->cart_repository = $cart_repository;
    }
    
    /**
     * Основная функция получения данных о товарах в корзине
     *
     * @return CartInfo  DTO-объект с данными корзины
     */
    public function getCartInfo(): CartInfo
    {
        $products = Yii::$app->session->get('products') ?? [];
        
        $items = $this->cart_repository->getItemsForCart(array_keys($products));
        $total_price = $this->getFullPrice($items, $products);
        
        return CartInfo::loadFromArray([
            'products'=>$products,
            'items'=>$items,
            'total_price'=>$total_price,
            'quantity'=>$this->countItems($products)
        ]);
    }
    
    /**
     * Метод добавления товара в корзину
     *
     * @param int $id  ID добавляемого товара
     * @return int  Общее количество товаров в корзине (для вывода в кнопке корзины)
     */
    public function addItemIntoCart(int $id): int
    {
        $products = Yii::$app->session->get('products') ?? [];

        if (array_key_exists($id, $products)) {
            $products[$id]++;
        } else {
            $products[$id] = 1;
        }

        Yii::$app->session->set('products', $products);

        return $this->countItems($products);
    }
    
    /**
     * Метод удаления товара из корзины
     *
     * @param int $id  ID удаляемого товара
     * @return int  Общее количество товаров в корзине (для вывода в кнопке корзины)
     */
    public function deleteItemFromCart(int $id): int
    {
        $products = Yii::$app->session->get('products') ?? [];

        if (array_key_exists($id, $products)) {
            $products[$id]--;

            if ($products[$id] <= 0) {
                unset($products[$id]);
            }
        }

        Yii::$app->session->set('products', $products);

        return $this->countItems($products);
    }
    
    /**
     * Метод получения общей цены товаров в корзине
     *
     * @param array $items  Массив с характеристиками товаров
     * @param array $products  Продукты в корзине и их количества
     * @return int  Общая цена товаров в корзине
     */
    public function getFullPrice(array $items, array $products): int
    {
        $total_price = 0;

        if (count($items)>0) {
            foreach($items as $item) {
                $total_price += $item->price * $products[$item->id];
            }
        }

        return $total_price;
    }
    
    public function countItems(array $products): int
    {
        if (count($products)>0) {

            $count_products = 0;

            foreach($products as $id => $quantity) {
                $count_products += $quantity;
            }

            return $count_products;

        } else {

            return 0;
        }
    }
    
    public function getItemsForCart(array $products_ids): array
    {
        return $this->cart_repository->getItemsForCart($products_ids);
    }
}
