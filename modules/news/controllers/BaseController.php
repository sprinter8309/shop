<?php

namespace app\modules\news\controllers;

use yii\web\Controller;
use app\modules\news\services\NewsService;
use app\modules\news\services\CommentsService;
use app\modules\news\serializers\NewsItemSerializer;

/**
 * Базовый контроллер для модуля новостей
 * 
 * @author Oleg Pyatin
 */
class BaseController extends Controller
{
    public function __construct($id, $module,
            protected NewsService $news_service,
            protected CommentsService $comments_service,
            $config = [])
    {
        $this->news_service = $news_service;
        $this->comments_service = $comments_service;
        
        parent::__construct($id, $module, $config);
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index', [
            'news_data_provider'=>$this->news_service->getNewsListProvider()
        ]);
    }
    
    public function actionView($id): string
    {
        return $this->render('news_detail',
            ['news_item' => NewsItemSerializer::serialize($this->news_service->getNewsItem($id)), 
             'news_comments' => $this->comments_service->getCommentsByNewsId($id), 
             'comments_item' => $this->comments_service->getNewComment()]
        );
    }
}
