<?php

namespace app\modules\news\services;

use yii\base\Component;
use app\models\Comments;
use app\modules\news\repositories\CommentsRepository;

/**
 * Сервис работы с комментариями к новостям
 * 
 * @author Oleg Pyatin
 */
class CommentsService extends Component
{
    public function __construct(protected CommentsRepository $comments_repository)
    {
        $this->comments_repository = $comments_repository;
    }
    
    public function getNewComment(): string
    {
        return $this->comments_repository->getNewComment("word ");
    }
    
    public function getCommentsByNewsId($news_id): array
    {
        $comments = $this->comments_repository->getCommentsByNewsId($news_id);
        foreach ($comments as &$comment) {
            $bufDateTime = new \DateTime($comment->create_datetime);
            $bufString = $bufDateTime->format('d M Y H:i');
            $comment->create_datetime = $bufString;
                    
            // Сделать учет update_datetime (если не null - выводим его)
            // Может в yii2 есть метод для применения функции к массиву (в ArrayHelper)
        }
        return $comments;
    }    
}
