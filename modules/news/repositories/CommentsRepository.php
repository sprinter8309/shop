<?php

namespace app\modules\news\repositories;

use app\models\Comments;
use yii\db\ActiveQuery;

/**
 * Класс репозиторий для данных комментариев к новостям
 * 
 * @author Oleg Pyatin
 */
class CommentsRepository
{
    public function getNewComment(string $new_comment): string
    {
        return $new_comment . " comment from repository";
    }
    
    public function getCommentsByNewsId($news_id): array
    {
        return Comments::findAll(['news_id'=>$news_id]);
    }
}
