<?php

namespace app\modules\news\serializers;

use app\models\News;
use app\components\serializers\AbstractProperties;

class NewsItemSerializer extends AbstractProperties
{
    public function getProperties(): array 
    {
        return [
            News::class => [
                'name',
                'image',
                'detail_text',
                'creation_date',
            ]
        ];
    }
}
