<?php

namespace app\modules\order\factories;

use app\helpers\DateHelper;
use app\models\Orders;
use app\components\constants\OrderConst;
use app\modules\order\forms\OrderCreateForm;
use yii\helpers\Json;

/**
 * Класс фабрика для создания заказов
 * 
 * @author Oleg Pyatin
 */
class OrderFactory
{
    public function createOrder(OrderCreateForm $order_create_form, int $user_id, array $products, int $new_order_id): Orders
    {
        $new_order = new Orders();
        
        $new_order->id = $new_order_id; 
        $new_order->products = Json::encode($products);
        $new_order->date_creation = DateHelper::now(); 
        $new_order->user_comment = $order_create_form->user_comment;
        $new_order->user_id = $user_id;
        $new_order->user_name = $order_create_form->user_name;
        $new_order->user_phone = $order_create_form->user_phone;
        $new_order->order_status = OrderConst::ORDER_STATUS_PROCESSING_WAIT;
        
        return $new_order;
    }
}
