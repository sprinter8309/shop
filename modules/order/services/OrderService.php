<?php

namespace app\modules\order\services;

use Yii;
use app\components\constants\OrderConst;
use app\modules\cart\services\CartService;
use app\modules\order\repositories\OrderRepository;
use app\modules\order\factories\OrderFactory;
use app\modules\order\forms\OrderCreateForm;
use app\modules\order\entities\CreateOrderViewData;

/**
 * Сервис для работы с заказами
 * 
 * @author Oleg Pyatin
 */
class OrderService
{
    public function __construct(protected OrderRepository $order_repository, protected OrderFactory $order_factory,
                                protected CartService $cart_service)
    {
        $this->order_repository = $order_repository;
        $this->order_factory = $order_factory;
        $this->cart_service = $cart_service;
    }
    
    public function createOrderBase(): CreateOrderViewData
    {
        $create_form = new OrderCreateForm();
        $result = false;
        $quantity_items = 0;
        $total_price = 0;
        $items = [];
        
        $products = Yii::$app->session->get('products') ?? [];
        if (!empty($products)) {
            $items = $this->cart_service->getItemsForCart(array_keys($products)); 
            $quantity_items = $this->cart_service->countItems($products);
            $total_price = $this->cart_service->getFullPrice($items, $products); 
        }
        
        return CreateOrderViewData::loadFromArray([
            'model' => $create_form,
            'result' => $result,
            'products_cart' => $products,
            'items' => $items,
            'quantity_items' => $quantity_items,
            'total_price' => $total_price,
        ]);    
    }
    
    public function createOrderSend(): CreateOrderViewData
    {
        $create_form = new OrderCreateForm();
        $result = false;
        $quantity_items = 0;
        $total_price = 0;
        $products = Yii::$app->session->get('products') ?? [];
        $items = $this->cart_service->getItemsForCart(array_keys($products));
        
        $create_form->load(Yii::$app->request->bodyParams);
        
        if ($create_form->validate()) {

            $new_order = $this->order_factory->createOrder($create_form, Yii::$app->user->id,
                    $products, $this->order_repository->getNextOrdersId());
            try {
                
                $this->order_repository->save($new_order); 
                Yii::$app->session->remove('products');  
                $result = true;
                
            } catch (\Throwable) {
                
                $create_form->addError('user_comment', OrderConst::ORDER_ERROR_SAVE_MESSAGE);
                $quantity_items = $this->cart_service->countItems($products);
                $total_price = $this->cart_service->getFullPrice($items, $products); 
            }
            
        } else {
                
            // Получаем id товаров, их список, количество и общую стоимость
            $quantity_items = $this->cart_service->countItems($products);
            $total_price = $this->cart_service->getFullPrice($items, $products); 
        }
        
        return CreateOrderViewData::loadFromArray([
            'model' => $create_form,
            'result' => $result,
            'products_cart' => $products,
            'items' => $items,
            'quantity_items' => $quantity_items,
            'total_price' => $total_price,
        ]);
    }
    
    public function getOrdersInfo(array $orders): array
    {
        $orders_info = [];
        
        foreach($orders as $order) {

            $products = json_decode($order['products'], true);
            $items = $this->cart_service->getItemsForCart(array_keys($products));
            $total_price = $this->cart_service->getFullPrice($items, $products);

            $orders_info[$order['id']] = [
                'items' => $items,
                'total_price' => $total_price,
                'products_cart' => $products
            ];
        }
        
        return $orders_info;
    }
}
