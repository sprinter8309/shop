<?php

namespace app\modules\order\controllers;

use yii\web\Controller;
use app\modules\order\services\OrderService;

/**
 * Default controller for the `order` module
 */
class BaseController extends Controller
{
    public function __construct($id, $module,
            protected OrderService $order_service,
            $config = [])
    {
        $this->order_service = $order_service;
        
        parent::__construct($id, $module, $config);
    }
    
    public function actionOrderBase(): string
    {
        $order_create_data = $this->order_service->createOrderBase();
        
        return $this->render('order', [
            'model' => $order_create_data->model,
            'result' => $order_create_data->result,
            'products_cart' => $order_create_data->products_cart,
            'items' => $order_create_data->items,
            'quantity_items' => $order_create_data->quantity_items,
            'total_price' => $order_create_data->total_price,
        ]);
    }
    
    public function actionOrderSend(): string
    {
        $order_create_data = $this->order_service->createOrderSend();
        
        return $this->render('order', [
            'model' => $order_create_data->model,
            'result' => $order_create_data->result,
            'products_cart' => $order_create_data->products_cart,
            'items' => $order_create_data->items,
            'quantity_items' => $order_create_data->quantity_items,
            'total_price' => $order_create_data->total_price,
        ]);        
    }
}
