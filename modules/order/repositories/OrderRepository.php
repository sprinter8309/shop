<?php

namespace app\modules\order\repositories;

use yii\db\Query;
use app\components\repositories\BaseRepository;

/**
 * Класс репозиторий для данных заказов
 * 
 * @author Oleg Pyatin
 */
class OrderRepository extends BaseRepository
{
    public function getNextOrdersId(): int
    {
        return ((new Query())->from('orders')->max('id')) + 1;
    }
}
