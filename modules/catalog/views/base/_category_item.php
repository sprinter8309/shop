<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Category;
Yii::$app->session['category_count'] = Yii::$app->session['category_count'] + 1;
?>

<?php if(Category::existChildCategory($model->id)): ?>
    <div class="catalog-index-item">
        <div class="catalog-index-item-image">
                <?= Html::a( Html::img( '/site/wb-image/'.(explode('.', $model->image)[0])) ,
                            Url::to(['/catalog/category/'.$model->url_name], ['class'=>'catalog-index-path']) ); ?>
        </div>
        <div class="catalog-index-item-name">
                <?= Html::a( $model->name, Url::to(['/catalog/category/'.$model->url_name], ['class'=>'catalog-index-path']) ); ?>
        </div>
        <div class="catalog-index-list-name">
                <?= Html::a( 'Смотреть в каталоге', Url::to(['/catalog/'.$model->url_name], ['class'=>'catalog-index-list-name']) ); ?>
        </div>
    </div>
<?php else: ?>
    <div class="catalog-index-item">
        <div class="catalog-index-item-image no-select">
                <?= \yii\helpers\Html::img( Yii::getAlias('@upload/'.$model->image) ) ?>
        </div>
        <div class="catalog-index-item-name no-select">
                <?= $model->name ?>
        </div>
        <div class="catalog-index-list-name">
                <?= Html::a( 'Смотреть в каталоге', Url::to(['/catalog/'.$model->url_name], ['class'=>'catalog-index-list-name']) ); ?>
        </div>
    </div>
<?php endif; ?>

<?php
if( Yii::$app->session['category_count'] === 2) {
    echo '<div class="catalog-item-divide clearfix">';
    Yii::$app->session['category_count'] = 0;
    echo '</div>';
}
?>
