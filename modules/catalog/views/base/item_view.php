<?php
$this->title = $item['name'];
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = [
    'template' => '<li>{link}</li> ',
    'label' => 'Каталог',
    'url' => [ Url::to(['/catalog'])]
];

foreach($category_path as $path_item) {
    $this->params['breadcrumbs'][] = [
        'template' => ' <span class="breadcrumbs-divide">&gt;</span> <li>{link}</li>',
        'label' => $path_item['name'],
        'url' => [ Url::to(['/catalog/'.$path_item['url_name']])]
    ];
}

$this->params['breadcrumbs'][] = [
    'template' => '<span class="breadcrumbs-divide">&gt;</span> <li>{link}</li>',
    'label' => strip_tags($item['name'])
];
?>

<div>
    <?php
        echo Breadcrumbs::widget([
            'options' => [
                'class' => 'breadcrumbs-path'
            ],
            'homeLink'=> false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [ ]
        ]);
    ?>
</div>

<div class="catalog-detail-name-block">
    <?= $item['name'] ?>
</div>

<div class="catalog-detail-container clearfix">
    <div class="catalog-detail-image">
        
        <?= Html::img( '/site/wb-image/'.(explode('.', $item['image'])[0])) ?>
    </div>
    <div class="catalog-detail-text">
        <div class="catalog-detail-price"><?= $item['price'] ?> руб.</div>
        <div class="catalog-detail-buttons-block">
            <button class="catalog-item-buy" item-code="<?= $item['id'] ?>">В корзину</button>
        </div>
    </div>
</div>
<div class="catalog-detail-offset"></div>
<div class="catalog-detail-text-container">
    <?= $item['full_description'] ?>

    <div class="catalog-detail-return">
        <?php  echo Html::a('Вернуться в каталог', Url::to(['/catalog']) );  ?>
    </div>
</div>
