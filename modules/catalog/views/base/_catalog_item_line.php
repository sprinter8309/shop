<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="catalog-item-line">
    <div class="catalog-item-line-container clearfix">
        <div class="catalog-item-line-image-container">
            <div class="catalog-item-line-image">
                <a href="<?= '/catalog/item/'.$model->id ?>"><?= Html::img( '/site/wb-image/'.(explode('.', $model->image)[0])) ?></a>
            </div>
        </div>
        <div class="catalog-item-line-info">
            <div class="catalog-item-line-title clearfix">
                <a href="<?= '/catalog/item/'.$model->id ?>"><?=$model-> name ?></a>
            </div>
            <div class="catalog-item-line-rating">
                <span class="catalog-item-rating-block">
                    <?php
                        $quant_of_rating_stars = ($model->rating < 3.8) ? (int)floor($model->rating) : (int)round($model->rating);
                        for ($i=0; $i<$quant_of_rating_stars; $i++) {
                            echo '<i>&#9733;</i>';
                        }
                        for ($i=$quant_of_rating_stars; $i<5; $i++) {
                            echo '&#9733;';
                        }
                    ?>
                </span>
            </div>
            <div class="catalog-item-line-text">
                <?= $model->short_description ?>
                <a href="<?= '/catalog/item/'.$model->id ?>"> Подробнее...</a>
            </div>

        </div>
        <div class="catalog-item-line-button">
            <button class="catalog-item-buy" item-code="<?= $model->id ?>">В корзину</button>
        </div>
        <div class="catalog-item-line-price">
            <?= $model->price ?> р.
        </div>
    </div>
</div>
