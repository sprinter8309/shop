<?php
$this->title = 'Каталог';
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\components\enums\CatalogShowType;

$this->params['breadcrumbs'][] = [
    'template' => '<li>{link}</li> ',
    'label' => 'Каталог',
    'url' => [ Url::to(['/catalog'])]
];

foreach( $category_path as $path_item) {
    $this->params['breadcrumbs'][] = [
        'template' => ' <span class="breadcrumbs-divide">&gt;</span> <li>{link}</li>',
        'label' => strip_tags($path_item['name']),
        'url' => [ Url::to(['/catalog/'.$path_item['url_name']])]
    ];
}

$this->params['breadcrumbs'][] = [
    'template' => '<span class="breadcrumbs-divide">&gt;</span> <li>{link}</li>',
    'label' => $category_name
];

if (!empty($producers_names_list)) {
    $this->params['producers_list'] = $producers_names_list;
}

if (!empty($exist_active_filter)) {
    $this->params['active_filter'] = true;
}

?>

<?= Html::jsFile('@web/js/main.js') ?>

<style>
    .pagination-view-type {
        color: #ccc;
        margin-left: 10px;
        /*border: 1px solid blue;*/
        text-align: right;
    }

    .pagination-view-type svg {
        fill: currentColor;
        stroke: currentColor;
        width: 27px;
        height: 26px;
        cursor: pointer;
        margin-left: 11px;
        stroke-width: 4px;
        /*border: 1px solid blue;*/
    }

    .pagination-view-type span {
        transition: color 0.2s;
    }
    .pagination-view-type span:hover {
        color: #2ba;
    }

    .bold-line {
        stroke-width: 10px;
    }
</style>

<div>
    <?php
        echo Breadcrumbs::widget([
            'options' => [
                'class' => 'breadcrumbs-path'
            ],
            'homeLink'=> false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [ ]
        ]);
    ?>
</div>

<h2><?= $category_name ?></h2>

<div class="catalog-items-tile">
<?php
    Yii::$app->session->set('item_count', 0);

    $option_list = '';
    $option_quants = [5, 10, 15, 20, 30];

    // Делаем отметку какой пункт в combobox - Должен быть выделен
    for ($i=0; $i<5; $i++) {
        if ($option_quants[$i]!==(int)$pagination_quantity) {
            $option_list.='<option>'.$option_quants[$i].'</option>';
        } else {
            $option_list.='<option selected>'.$option_quants[$i].'</option>';
        }
    }

    if ($show_type===CatalogShowType::Line) {
        $item_view_file = '_catalog_item_line';
    } elseif ($show_type===CatalogShowType::Tile) {
        $item_view_file = '_catalog_item';
    }

    echo ListView::widget([
        'dataProvider' => $catalog_data_provider,
        'itemView' => $item_view_file,
        'options' => [ ],
        'summary' => 'Товары <b>{begin}</b>...<b>{end}</b>, всего - {totalCount}',
        'layout' => "<div class='catalog-tile-summary'>{summary}</div>"
        . "<div class='catalog-tile-options clearfix'>
                <div class='sort-choice'>
                    <span class='sort-choice-label'>Сортировать по:</span>
                    <span class='sort-choice-price'>Цене</span>
                    <span class='sort-choice-rating'>Рейтингу</span>
                </div>
                <div class='pagination-view-type'>
                    <span class='pagination-view-type-line'><svg>
                        <line x1='1' y1='3' x2='5' y2='3'/>
                        <line x1='1' y1='10' x2='5' y2='10'/>
                        <line x1='1' y1='17' x2='5' y2='17'/>
                        <line x1='1' y1='24' x2='5' y2='24'/>

                        <line x1='9' y1='3' x2='27' y2='3'/>
                        <line x1='9' y1='10' x2='27' y2='10'/>
                        <line x1='9' y1='17' x2='27' y2='17'/>
                        <line x1='9' y1='24' x2='27' y2='24'/>
                    </svg></span><span class='pagination-view-type-tile'><svg>
                        <line class='bold-line' x1='0' y1='5' x2='11' y2='5'/>
                        <line class='bold-line' x1='0' y1='6' x2='11' y2='6'/>

                        <line class='bold-line' x1='15' y1='5' x2='26' y2='6'/>
                        <line class='bold-line' x1='15' y1='6' x2='26' y2='6'/>

                        <line class='bold-line' x1='0' y1='20' x2='11' y2='20'/>
                        <line class='bold-line' x1='0' y1='21' x2='11' y2='21'/>

                        <line class='bold-line' x1='15' y1='20' x2='26' y2='20'/>
                        <line class='bold-line' x1='15' y1='21' x2='26' y2='21'/>
                    </svg>
                </div>
                <select class='pagination-choice'>".
                    $option_list
                ."</select>
                <div class='label-pagination-choice'>
                    Показывать по:
                </div>
            </div>\n{items}\n
                        <div class='catalog-item-divide clearfix'></div><div class='catalog-tile-pager'>{pager}</div>"
    ]);
    Yii::$app->session->remove('item_count');
?>
</div>
