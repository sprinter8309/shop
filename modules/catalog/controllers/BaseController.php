<?php

namespace app\modules\catalog\controllers;

use yii\web\Controller;
use yii\base\View;
use app\modules\catalog\services\CatalogService;
use app\modules\catalog\serializers\CatalogItemSerializer;

/**
 * Default controller for the `catalog` module
 */
class BaseController extends Controller
{
    public function __construct($id, $module,
            protected CatalogService $catalog_service,
            $config = [])
    {
        $this->catalog_service = $catalog_service;
        
        parent::__construct($id, $module, $config);
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex(?string $category = null): string
    {
        $parent_url = '';
        
        $this->catalog_service->clearCatalogFilters();
        
        return $this->render('index', [
            'catalog_index_data_provider' => $this->catalog_service->getCatalogIndexProvider($category), 
            'parent_url' => $parent_url
        ]);
    }
    
    public function actionCategory(string $category): string
    {
        $catalog_category_data = $this->catalog_service->getCatalogCategoryData($category);
        
        return $this->render('catalog', [
            'catalog_data_provider' => $catalog_category_data->catalog_data_provider,
            'category_name' => $catalog_category_data->category_name, 
            'category_path' => $catalog_category_data->category_path, 
            'pagination_quantity' => $catalog_category_data->pagination_quantity,
            'show_type' => $catalog_category_data->show_type,
            'producers_names_list' => $catalog_category_data->producers_names_list
        ]);
    }
    
    public function actionView(string $id): string
    {
        return $this->render('item_view', CatalogItemSerializer::serialize($this->catalog_service->getCatalogItemData($id)));
    }
    
    public function actionUpdate(): string
    {
        $catalog_list_view_data = $this->catalog_service->getUpdatedCatalogData();
        
        return (new View())->render('@app/modules/catalog/views/base/_catalog_updated_list.php', [
            'catalog_data_provider'=>$catalog_list_view_data->catalog_data_provider,
            'item_view_file'=>$catalog_list_view_data->item_view_file,
            'option_list'=>$catalog_list_view_data->option_list
        ]);
    }
}
