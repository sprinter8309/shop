<?php

namespace app\modules\catalog\repositories;

use app\models\Category;
use app\models\Items;
use app\models\Producers;
use yii\db\ActiveQuery;

/**
 * Класс репозиторий для данных каталога
 * 
 * @author Oleg Pyatin
 */
class CatalogRepository
{
    public function getIndexCategoryQuery(): ActiveQuery
    {
        return Category::find()->where(['level_cat' => 1]);
    }
    
    public function getCategoryByUrlName(string $category): ?Category
    {
        return Category::find()->where(['url_name' => $category])->one();
    }
    
    public function getCategoryById(string $id): ?Category
    {
        return Category::find()->where(['id' => $id])->one();
    }
    
    public function getCategoryItemsQuery(Category $parent_category): ActiveQuery
    {
        return Category::find()->where(['parent_cat' => $parent_category['id']]);
    }
    
    public function getItemsForSelectedCategories(array $categories_ids_list): array
    {
        return Items::find()->where(['category_id'=>$categories_ids_list])->all();
    }
    
    public function getProducersByIds(array $producers_unique_ids_set): array
    {
        return Producers::find()->where(['in', 'id', $producers_unique_ids_set])->all();
    }
    
    public function getAllCategories(): array
    {
        return Category::find()->all();
    }
    
    public function getItemsByCategoriesIdsQuery(array $category_actual_ids, string $sort_method): ActiveQuery
    {
        return Items::find()->where(['category_id'=>$category_actual_ids])->orderBy($sort_method);
    }
    
    public function getItemById(string $id): ?Items
    {
        return Items::findOne(['id'=>$id]);
    }
    
    public function getItemsByConditionListQuery(array $condition_list, string $sort_method): ActiveQuery
    {
        return Items::find()->where($condition_list)->orderBy($sort_method);
    }
    
    public function getProducerByName(string $producer_name): ?Producers
    {
        return Producers::find()->where(['name'=>$producer_name])->one();
    }
}
