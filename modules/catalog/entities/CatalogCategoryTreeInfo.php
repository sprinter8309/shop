<?php

namespace app\modules\catalog\entities;

use app\components\dto\AbstractDto;

/**
 * DTO-класс для хранения информации о категориях связанных с текущей
 * 
 * @author Oleg Pyatin
 */
class CatalogCategoryTreeInfo extends AbstractDto
{
    public array $category_actual_ids;
    
    public string $category_name;
    
    public array $category_path;
}
