<?php

namespace app\models;

use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    // Функция проверки - есть ли у указанной категории какое-либо количество дочерних
    public static function existChildCategory($id): bool
    {
        $exist = Category::find()->where(['parent_cat'=>$id])->one();

        if (!empty($exist)) {
            return true;
        } else {
            return false;
        }
    }
}
