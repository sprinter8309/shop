<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\filters\VerbFilter;
use app\models\Orders;
use app\components\extensions\BaseActiveRecord;

class User extends BaseActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            ['login', 'required', 'message' => 'Не введен логин' ],
            ['password', 'required', 'message' => 'Не введен пароль' ],
            ['password', 'string', 'min' => 5, 'message' => 'Пароль минимум от 5 символов'],
            ['email', 'email', 'message' => 'E-mail неправильного формата']
        ];
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id): ?User
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null): ?User
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    public static function findIdentityByLogin($login): ?User
    {
        return static::findOne(['login'=>$login]);
    }
    

    /**
     * {@inheritdoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey(): string
    {
        return $this->auth_key;
    }

    public function generateAuthKey(): void
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password): bool
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }
    
    public function setPassword($password): void
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
    
    public function changePasswordIfIsActual(string $new_password): void
    {
        if (strlen($new_password)>0 && !Yii::$app->security->validatePassword($new_password, $this->password)) {
            $this->password = Yii::$app->security->generatePasswordHash($new_password);
        }
    }
    
    public function getOrders()
    {
        return $this->hasMany(Orders::class, ['user_id'=>'id']);
    }
}
