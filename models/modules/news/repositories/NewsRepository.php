<?php

namespace app\modules\news\repositories;

use app\models\News;
use yii\db\ActiveQuery;
use app\components\constants\DisplayConst;

/**
 * Класс репозиторий для данных из раздела новостей
 * 
 * @author Oleg Pyatin
 */
class NewsRepository
{
    public function getIndexNewsQuery(): ActiveQuery
    {
        return News::find();
    }
    
    public function getNewsById($id): ?News
    {
        return News::findOne(['id'=>$id]);
    }
    
    public function getNewsForIndexPage(): array
    {
        return News::find()->orderBy(['id'=>'SORT_DESC'])->limit(DisplayConst::INDEX_PAGE_NEWS_LIST_QUANTITY)->all();
    }
}
