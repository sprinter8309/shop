<?php

namespace app\modules\news\controllers;

use Yii;

use yii\web\Controller;
use app\modules\news\services\NewsService;
use app\modules\news\serializers\NewsItemSerializer;

/**
 * Базовый контроллер для модуля новостей
 * 
 * @author Oleg Pyatin
 */
class BaseController extends Controller
{
    public function __construct($id, $module,
            protected NewsService $news_service,
            $config = [])
    {
        $this->news_service = $news_service;
        
        parent::__construct($id, $module, $config);
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index', [
            'news_data_provider'=>$this->news_service->getNewsListProvider()
        ]);
    }
    
    public function actionView($id): string
    {
        //echo strlen('$2y$13$w3.t49JixVszUfBoMY1ZyeawuZCHrk5OAULZS0Jxh33pVg8Goeueq');//Yii::$app->security->generatePasswordHash("test");
        echo Yii::$app->security->generatePasswordHash("test");
        exit();
        
        return $this->render('news_detail',
            ['news_item' => NewsItemSerializer::serialize($this->news_service->getNewsItem($id))]
        );
    }
}
