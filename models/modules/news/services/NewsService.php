<?php

namespace app\modules\news\services;

use yii\base\Component;
use yii\data\ActiveDataProvider;
use app\models\News;
use app\components\constants\DisplayConst;
use app\modules\news\repositories\NewsRepository;

/**
 * Основной сервис работы с разделом новостей
 * 
 * @author Oleg Pyatin
 */
class NewsService extends Component
{
    public function __construct(protected NewsRepository $news_repository)
    {
        $this->news_repository = $news_repository;
    }
    
    public function getNewsListProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([

            'query' => $this->news_repository->getIndexNewsQuery(),
            'pagination' => [
                'pageSize' => DisplayConst::NEWS_LIST_QUANTITY
            ]
        ]);
    }
    
    public function getNewsItem($id): ?News
    {
        return $this->news_repository->getNewsById($id);
    }
    
    public function getNewsForIndexPage(): array
    {
        return $this->news_repository->getNewsForIndexPage();
    }
}
