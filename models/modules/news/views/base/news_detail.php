<?php
$this->title = $news_item['name'];
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = [
    'template' => '<li>{link}</li> ',
    'label' => 'Новости',
    'url' => [ Url::to(['/news'])]
];

$this->params['breadcrumbs'][] = [
    'template' => '<span class="breadcrumbs-divide">&gt;</span> <li>{link}</li>',
    'label' => $news_item['name']
];
?>

<div class="news-detail-container clearfix">
    <div class="breadcrumbs-large-container">
        <?php
            echo Breadcrumbs::widget([
                'options' => [
                        'class' => 'breadcrumbs-path'
                ],
                'homeLink'=> false,
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [ ]
            ]);
        ?>
    </div>
    <div class="news-detail-image">
        <?= Html::img( '/site/wb-image/'.(explode('.', $news_item['image'])[0]) ) ?>
    </div>
    <div class="news-detail-text">
        <div class="news-detail-name-block">
                <?= $news_item['name'] ?>
        </div>
        <div class="news-detail-date">
                25 Января 2018
        </div>
                <?= $news_item['detail_text'] ?>
        <div class="news-detail-separator"></div>
        <div class="news-detail-return">
                <?php  echo Html::a('Перейти к списку новостей', Url::to(['/news']) );  ?>
        </div>
    </div>
</div>
