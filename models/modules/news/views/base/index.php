<?php
$this->title = 'Новости';
use yii\widgets\ListView;
?>

<h2>Новости</h2>

<div class="news-container">
    <?php
        echo ListView::widget([
            'dataProvider' => $news_data_provider,
            'itemView' => '_news_item',
            'summary' => 'Новости <b>{begin}</b>...<b>{end}</b> из {totalCount}',
            'layout' => "<div class='news-tile-summary'>{summary}</div>\n{items}\n
                <div class='news-item-divide clearfix'></div><div class='news-tile-pager'>{pager}</div>"
        ]);
    ?>
</div>
