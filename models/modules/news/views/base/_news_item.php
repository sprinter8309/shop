<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="news-item clearfix">
    <div class="news-item-image">
        <?= Html::a( Html::img( '/site/wb-image/'.(explode('.', $model->image)[0]) ),
                Url::to(['/news/'.$model->id]) ); ?>
    </div>
    <div class="news-item-block">
        <?= Html::a( $model->name, Url::to(['/news/'.$model->id]), ['class'=>'news-item-link']); ?>
        <div class="news-item-date">
                25 Января 2018
        </div>
        <div class="news-item-text">
                <?= $model->preview_text ?>
        </div>
    </div>
</div>
