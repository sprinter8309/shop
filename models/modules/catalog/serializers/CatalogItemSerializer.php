<?php

namespace app\modules\catalog\serializers;

use app\models\Items;
use app\components\serializers\AbstractProperties;

class CatalogItemSerializer extends AbstractProperties
{
    public function getProperties(): array 
    {
        return [
            Items::class => [
                'id',
                'name',
                'index_code',
                'price',
                'image',
                'full_description',
                'availability',
                'is_new',
                'is_recommended',
                'is_hit',
                'rating',
                'status',
            ],
        ];
    }
}
