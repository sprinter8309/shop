<?php

namespace app\modules\catalog\services;

use Yii;
use yii\base\Component;
use yii\data\ActiveDataProvider;
use app\components\constants\DisplayConst;
use app\components\NoReloadUrlManager;
use app\components\enums\CatalogShowType;
use app\components\enums\CatalogUpdateType;
use app\modules\catalog\entities\CatalogListViewData;
use app\modules\catalog\entities\CatalogCategoryTreeInfo;
use app\modules\catalog\repositories\CatalogRepository;

/**
 * Сервис для функциональности обновления каталога
 * 
 * @author Oleg Pyatin
 */
class CatalogUpdateService extends Component
{
    public function __construct(protected CatalogRepository $catalog_repository)
    {
        $this->catalog_repository = $catalog_repository;
    }
    
    public function getUpdatedCatalogData(CatalogCategoryTreeInfo $categories_info): CatalogListViewData
    {
        Yii::$app->session->set('item_count', 0); // Обновляем вывод плитки
        $update_action_type = $this->findoutUpdateType();
        
        $data_provider = $this->processUpdateActions($update_action_type, $categories_info); 

        $show_type = CatalogShowType::tryFrom(Yii::$app->session->get('show_type')) ?? DisplayConst::DEFAULT_SHOW_TYPE; 
        $pagination =  Yii::$app->session->get('pagination') ?? DisplayConst::DEFAULT_PAGINATION_SIZE;
        
        return CatalogListViewData::loadFromArray([
            'catalog_data_provider'=>$data_provider,
            'item_view_file'=>$this->getActualTileType($show_type),
            'option_list'=>$this->generateOptionList((int)$pagination),
        ]);
    }
    
    public function findoutUpdateType(): CatalogUpdateType
    {
        $type = Yii::$app->request->get('type');
        
        if (isset($type)) {
            return match($type) {
                'pagination'=>CatalogUpdateType::Pagination,
                'sort'=>CatalogUpdateType::Sort,
                'display'=>CatalogUpdateType::Display,
                'filter'=>CatalogUpdateType::Filter,
                default=>CatalogUpdateType::Cancel,
            };
        } else {
            return CatalogUpdateType::Page;
        }
    }
    
    public function processUpdateActions(CatalogUpdateType $update_action_type, CatalogCategoryTreeInfo $categories_info): ActiveDataProvider
    {
        // Установка изменяемых параметров по-умолчанию
        $sort_method =  Yii::$app->session->get('method') ?? DisplayConst::DEFAULT_SORT_METHOD;
        
        $show_type = CatalogShowType::tryFrom(Yii::$app->session->get('show_type')) ?? DisplayConst::DEFAULT_SHOW_TYPE; 
        
        $condition_list = $this->getItemsConditionList($categories_info);
        
        $provider_pagination = [
            'pageSize'=>Yii::$app->session->get('pagination') ?? DisplayConst::DEFAULT_PAGINATION_SIZE,
            'urlManager'=>new NoReloadUrlManager()
        ];

        switch ($update_action_type) {
            case CatalogUpdateType::Pagination:
                
                $pagination = Yii::$app->request->get('quantity') ?? DisplayConst::DEFAULT_PAGINATION_SIZE;
                Yii::$app->session->set('pagination', $pagination);
                $provider_pagination['pageSize'] = $pagination;
                break;
                
            case CatalogUpdateType::Sort:
                
                $sort_method = Yii::$app->request->get('method') ?? DisplayConst::DEFAULT_SORT_METHOD;
                Yii::$app->session->set('sort_method', $sort_method);
                break;
            
            case CatalogUpdateType::Display:
                
                $show_type = CatalogShowType::tryFrom(Yii::$app->request->get('show')) ?? DisplayConst::DEFAULT_SHOW_TYPE; 
                Yii::$app->session->set('show_type', $show_type->stringName());
                break;
            
            case CatalogUpdateType::Filter:
                $condition_list = $this->changeItemsConditionList($condition_list);
                break;
            
            case CatalogUpdateType::Page:
                
                $url_parts = explode('/', Yii::$app->request->get('path'));
                $page_number = $url_parts[count($url_parts)-1];
                
                $provider_pagination += ['page'=>$page_number];
                break;
            default: 
                break;
        }
        
        return new ActiveDataProvider([
            'query' => $this->catalog_repository->getItemsByConditionListQuery($condition_list, $sort_method), 
            'pagination' => $provider_pagination
        ]);
    }
    
    public function getItemsConditionList(CatalogCategoryTreeInfo $categories_info): array
    {
        $condition_list = [
            'category_id' => $categories_info->category_actual_ids
        ];
        
        if (Yii::$app->session->has('producer_filter')) {
            $condition_list += [ 'producer_id'=>Yii::$app->session->get('producer_filter') ];
        }
        
        return $condition_list;
    }
    
    public function changeItemsConditionList(array $condition_list): array
    {
        if (empty( Yii::$app->request->get('discard') )) {
            $producer = $this->catalog_repository->getProducerByName( Yii::$app->request->get('producer') ); 
            Yii::$app->session->set('producer_filter', $producer->id);

            $condition_list['producer_id'] = $producer->id;

        } else {
            Yii::$app->session->remove('producer_filter');
            unset($condition_list['producer_id']);
        }
        
        return $condition_list;
    }
    
    public function getActualTileType(CatalogShowType $show_type): string
    {
        return match ($show_type) {
            CatalogShowType::Tile => '_catalog_item',
            CatalogShowType::Line => '_catalog_item_line',
            default => '_catalog_item'
        };
    }

    public function generateOptionList(int $pagination): string
    {
        $option_list = '';
        $option_quants = [5, 10, 15, 20, 30];

        for ($i=0; $i<5; $i++) {
            if ($option_quants[$i]!==$pagination) {
                $option_list.='<option>'.$option_quants[$i].'</option>';
            } else {
                $option_list.='<option selected>'.$option_quants[$i].'</option>';
            }
        }
        return $option_list;
    }
}
