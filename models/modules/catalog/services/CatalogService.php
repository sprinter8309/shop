<?php

namespace app\modules\catalog\services;

use Yii;
use yii\base\Component;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\components\constants\DisplayConst;
use app\components\BaseNoReloadUrlManager;
use app\components\enums\CatalogShowType;
use app\modules\catalog\{entities\CatalogItemData, 
                         entities\CatalogCategoryData, 
                         entities\CatalogCategoryTreeInfo, 
                         entities\CatalogListViewData, 
                         repositories\CatalogRepository, 
                         services\CatalogUpdateService};

/**
 * Основной сервис работы с каталогом
 * 
 * @author Oleg Pyatin
 */
class CatalogService extends Component
{
    public function __construct(protected CatalogRepository $catalog_repository, protected CatalogUpdateService $catalog_update_service)
    {
        $this->catalog_repository = $catalog_repository;
        
        $this->catalog_update_service = $catalog_update_service;
    }    
    
    public function getCatalogIndexProvider(?string $category): ActiveDataProvider
    {
        if (isset($category)) {
            $parent_category = $this->catalog_repository->getCategoryByUrlName($category); 
            $query_category = $this->catalog_repository->getCategoryItemsQuery($parent_category); 
        } else {
            $query_category = $this->catalog_repository->getIndexCategoryQuery(); 
        }
        
        return new ActiveDataProvider([
            'query' => $query_category,
            'pagination' => [
                'pageSize' => DisplayConst::CATEGORY_ITEM_LIST_QUANTITY
            ]
        ]);
    }
    
    public function getCatalogCategoryData(string $category): CatalogCategoryData
    {
        $certain_pagination = Yii::$app->session->get('pagination') ?? DisplayConst::DEFAULT_PAGINATION_SIZE;
        $sort_method =  Yii::$app->session->get('sort_method') ?? DisplayConst::DEFAULT_SORT_METHOD;
        $show_type =  CatalogShowType::tryFrom(Yii::$app->session->get('show_type')) ?? DisplayConst::DEFAULT_SHOW_TYPE; 

        $categories_info = $this->getSelectedCategories($category);
        
        $catalog_data_provider = new ActiveDataProvider([
            'query' =>$this->catalog_repository->getItemsByCategoriesIdsQuery($categories_info->category_actual_ids, $sort_method), 
            'pagination' => [
                'pageSize' => $certain_pagination,
                'urlManager'=> new BaseNoReloadUrlManager(),
            ]
        ]);
        
        return CatalogCategoryData::loadFromArray([
            'catalog_data_provider' => $catalog_data_provider,
            'category_name' => $categories_info->category_name,   
            'category_path' => $categories_info->category_path, 
            'pagination_quantity' => $certain_pagination,
            'show_type' => $show_type,
            'producers_names_list' => $this->getItemsProducersNames($categories_info)
        ]);
    }
    
    public function getCatalogItemData(string $id): CatalogItemData
    {
        $item = $this->catalog_repository->getItemById($id);
        
        $categories_info = $this->getSelectedCategories($this->catalog_repository->getCategoryById($item->category_id)->url_name);
        
        return CatalogItemData::loadFromArray([
            'item'=>$item,
            'category_path'=>$categories_info->category_path
        ]);
    }
    
    public function getUpdatedCatalogData(): CatalogListViewData
    {
        $category = $this->getCurrentCategoryFromPath();
        
        $categories_info = $this->getSelectedCategories($category);
        
        return $this->catalog_update_service->getUpdatedCatalogData($categories_info);
    }
    
    public function getCurrentCategoryFromPath(): string
    {
        $url_parts = explode('/', Yii::$app->request->get('path'));
        return $url_parts[count($url_parts)-2];
    }
    
    public function getItemsProducersNames(CatalogCategoryTreeInfo $categories_info): array   
    {
        $items_set = $this->catalog_repository->getItemsForSelectedCategories($categories_info->category_actual_ids);  
 
        $producers_id_set = [];
        foreach ($items_set as $item) {
            $producers_id_set[] = $item['producer_id'];
        }

        $producers_unique_ids_set = array_unique($producers_id_set);  
        return $this->catalog_repository->getProducersByIds($producers_unique_ids_set); 
    }
    
    private function getSelectedCategories(string $current_category_url): CatalogCategoryTreeInfo
    {
        $categories = ArrayHelper::toArray($this->catalog_repository->getAllCategories(), [
            'app\models\Category'=>[
                'id',
                'url_name',
                'parent_cat',
                'name',
            ]
        ]);
        
        $url_indexed_categories = ArrayHelper::index($categories, 'url_name');
        $id_indexed_categories = ArrayHelper::index($categories, 'id');
        
        $current_cat_id = $url_indexed_categories[$current_category_url]['id'];
        
        $category_path = $this->getCategoryPath($id_indexed_categories, $current_cat_id);

        $actual_category_ids = $this->getActualCategoryIds($categories, $current_cat_id);
        
        return CatalogCategoryTreeInfo::loadFromArray([
            'category_actual_ids'=>$actual_category_ids,
            'category_name'=>$url_indexed_categories[$current_category_url]['name'],
            'category_path'=>$category_path,
        ]);
    }
    
    // Получение составных частей пути для дальнейшего использования в хлебных крошках
    private function getCategoryPath(array $id_indexed_categories, int $current_cat_id): array
    {
        $category_path = []; // Массив путей для хлебных крошек
        $find_parent_cat = true; // Флаг остановки для поиска
        $cat_find = $id_indexed_categories[$current_cat_id]['parent_cat'];  // ID-текущей родительской категории
        $block_counter = 3; // Счетчик защиты от зацикливания
        $buf_full_item_url = ''; // Буферная переменная получения полного пути к URL категории
        
        while ($find_parent_cat && ($block_counter > 0)) {
            
            $parent_cat = $id_indexed_categories[$cat_find] ?? null;
            
            if (isset($parent_cat)) {

                $buf_full_item_url = $parent_cat['url_name'] . '/' . $buf_full_item_url;

                array_unshift($category_path, [
                    'name' => $parent_cat['name'],
                    'url_name' => $buf_full_item_url
                ]);

                $cat_find = $parent_cat['parent_cat'];

            } else {
                $find_parent_cat = false;
            }
            
            $block_counter--;
        }
        
        return $category_path;
    }
    
    // Получение id текущей категории и дочерних для дальнейшего получения списка акутальных производителей (к фильтрации)
    private function getActualCategoryIds(array $categories, int $current_cat_id): array
    {
        $child_categories = array_filter($categories, fn ($v) => $v["parent_cat"]===$current_cat_id, ARRAY_FILTER_USE_BOTH);
        
        $buf_category_ids = ArrayHelper::merge([$current_cat_id], ArrayHelper::getColumn($child_categories, 'id'));
        return array_values($buf_category_ids);
    }
    
    
    public function clearCatalogFilters(): void
    {
        // Устраняем фильтр по производителю
        if (Yii::$app->session->has('producer_filter')) {
            Yii::$app->session->remove('producer_filter');
        }
    }
}
