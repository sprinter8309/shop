<?php

namespace app\modules\catalog\entities;

use yii\data\ActiveDataProvider;
use app\components\dto\AbstractDto;
use app\components\enums\CatalogShowType;

/**
 * DTO-класс для хранения параметров вывода товаров в окне категории
 * 
 * @author Oleg Pyatin
 */
class CatalogCategoryData extends AbstractDto
{
    public ActiveDataProvider $catalog_data_provider;
    
    public string $category_name;
    
    public array $category_path;
    
    public string $pagination_quantity;
    
    public CatalogShowType $show_type;
    
    public array $producers_names_list;
}
