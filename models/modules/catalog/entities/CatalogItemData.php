<?php

namespace app\modules\catalog\entities;

use app\components\dto\AbstractDto;
use app\models\Items;

/**
 * DTO-класс для хранения параметров вывода конкретного товара
 * 
 * @author Oleg Pyatin
 */
class CatalogItemData extends AbstractDto
{
    public Items $item;
    
    public array $category_path;
}
