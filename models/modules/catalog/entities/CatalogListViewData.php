<?php

namespace app\modules\catalog\entities;

use app\components\dto\AbstractDto;
use yii\data\ActiveDataProvider;

/**
 * DTO-класс для хранения данных виджета товаров с обновленными параметрами
 * 
 * @author Oleg Pyatin
 */
class CatalogListViewData extends AbstractDto
{
    public ActiveDataProvider $catalog_data_provider;
    
    public string $item_view_file;
    
    public string $option_list;
}
