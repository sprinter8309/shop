<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;

echo ListView::widget([
    'dataProvider' => $catalog_data_provider,
    'itemView' => '@app/modules/catalog/views/base/'.$item_view_file,
    'options' => [ ],
    'summary' => 'Товары <b>{begin}</b>...<b>{end}</b>, всего - {totalCount}',
    'layout' => "<div class='catalog-tile-summary'>{summary}</div>"
    . "<div class='catalog-tile-options clearfix'>
            <div class='sort-choice'>
                <span class='sort-choice-label'>Сортировать по:</span>
                <span class='sort-choice-price'>Цене</span>
                <span class='sort-choice-rating'>Рейтингу</span>
            </div>
            <div class='pagination-view-type'>
                <span class='pagination-view-type-line'><svg>
                    <line x1='1' y1='3' x2='5' y2='3'/>
                    <line x1='1' y1='10' x2='5' y2='10'/>
                    <line x1='1' y1='17' x2='5' y2='17'/>
                    <line x1='1' y1='24' x2='5' y2='24'/>

                    <line x1='9' y1='3' x2='27' y2='3'/>
                    <line x1='9' y1='10' x2='27' y2='10'/>
                    <line x1='9' y1='17' x2='27' y2='17'/>
                    <line x1='9' y1='24' x2='27' y2='24'/>
                </svg></span><span class='pagination-view-type-tile'><svg>
                    <line class='bold-line' x1='0' y1='5' x2='11' y2='5'/>
                    <line class='bold-line' x1='0' y1='6' x2='11' y2='6'/>

                    <line class='bold-line' x1='15' y1='5' x2='26' y2='6'/>
                    <line class='bold-line' x1='15' y1='6' x2='26' y2='6'/>

                    <line class='bold-line' x1='0' y1='20' x2='11' y2='20'/>
                    <line class='bold-line' x1='0' y1='21' x2='11' y2='21'/>

                    <line class='bold-line' x1='15' y1='20' x2='26' y2='20'/>
                    <line class='bold-line' x1='15' y1='21' x2='26' y2='21'/>
                </svg>
            </div>
            <select class='pagination-choice'>".
                $option_list
            ."</select>
            <div class='label-pagination-choice'>
                Показывать по:
            </div>
        </div>\n{items}\n
        <div class='catalog-item-divide clearfix'></div><div class='catalog-tile-pager'>{pager}</div>
        <script>

        <script>"
]);
