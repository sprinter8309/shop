<?php
use yii\helpers\Html;
use yii\helpers\Url;
Yii::$app->session['item_count'] = Yii::$app->session['item_count'] + 1;
?>

<div class="catalog-item">
    <div class="catalog-item-image">
            <?= Html::a(
            Html::img( '/site/wb-image/'.(explode('.', $model->image)[0])),
            Url::to(['/catalog/item/'.$model->id]) ); ?>
    </div>
    <div class="catalog-item-name">
            <?= Html::a( $model->name, Url::to(['/catalog/item/'.$model->id])); ?>
    </div>
    <div class="catalog-item-text">
            <?= $model->short_description ?>
            <?= Html::a( ' Подробнее...', Url::to(['/catalog/item/'.$model->id])); ?>
    </div>
    <div class="catalog-item-rating-container">
        <span class="catalog-item-rating-block">
            <?php
                $quant_of_rating_stars = ($model->rating < 3.8) ? (int)floor($model->rating) : (int)round($model->rating);
                for ($i=0; $i<$quant_of_rating_stars; $i++) {
                    echo '<i>&#9733;</i>';
                }
                for ($i=$quant_of_rating_stars; $i<5; $i++) {
                    echo '&#9733;';
                }
            ?>
        </span>
    </div>
    <div class="catalog-item-price clearfix">
            <?= $model->price ?> р.
    </div>
    <div class="catalog-item-button">
            <button class="catalog-item-buy" item-code="<?= $model->id ?>">В корзину</button>
    </div>
</div>

<?php
if( Yii::$app->session['item_count'] === 3) {
    echo '<div class="catalog-item-divide clearfix">';
    Yii::$app->session['item_count'] = 0;
    echo '</div>';
}
?>
