<?php
$this->title = 'Каталог';
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
?>

<h2>Каталог</h2>

<div class="catalog-items-tile">
<?php
    Yii::$app->session->set('category_count', 0);
    echo ListView::widget([
        'dataProvider' => $catalog_index_data_provider,
        'itemView' => '_category_item',
        'options' => [ ],
        'layout' => "{items}\n<div class='catalog-item-divide clearfix'>"
    ]);
    Yii::$app->session->remove('category_count');
?>
</div>

<?php if (isset($parent_url)): ?>
    <div class="catalog-index-return">
        <?= Html::a( 'Вернуться в каталог', Url::to(['/catalog']) ); ?>
    </div>
<?php endif; ?>
