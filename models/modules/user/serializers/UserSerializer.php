<?php

namespace app\modules\user\serializers;

use app\models\Users;
use app\components\serializers\AbstractProperties;

class UserSerializer extends AbstractProperties
{
    public function getProperties(): array 
    {
        return [
            Users::class => [
                'login',
                'email',
                'phone',
                'last_visit',
                'date_creation',
                'status',
            ]
        ];
    }
}
