<?php
$this->title = 'Вход';

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

<div class="login-block">
    <?php if (Yii::$app->user->isGuest): ?>
        <h2>Войти в систему</h2>
        <div class="login-form">
            <?php
                $form = ActiveForm::begin([
                    'id' => 'login-form',
                ]) ?>
                <div class="login-form-item login-message-field">
                    <ul>
                        <?php foreach($model->errors as $error): ?>
                                <li><?= $error[0] ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="login-form-item">
                        <label for="user-login">Введите свой логин</label>
                        <?= $form->field($model, 'login', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'Логин', 'value'=> $model->login ])->hint('') ?>
                </div>
                <div class="login-form-item">
                        <label for="login-password-field">Введите свой пароль</label>
                        <?= $form->field($model, 'password', ['enableLabel'=>false, 'enableError'=>false])->passwordInput(['placeholder'=>'Пароль', 'value'=> $model->password])->hint('') ?>
                </div>
                <div class="login-form-item">
                        <?= Html::submitButton('Вход', ['class' => 'user-form-submit']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    <?php else: ?>
        <h2>Вы уже в системе</h2>
        <div>
                <?= Html::a( 'Перейти в личный кабинет', Url::to(['/cabinet']) ); ?>
        </div>
    <?php endif; ?>
</div>




















