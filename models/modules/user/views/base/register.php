<?php
$this->title = 'Регистрация';

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

<div class="register-block">
    <?php if (Yii::$app->user->isGuest): ?>
    <h2>Регистрация</h2>
    <div class="register-form">
        <?php
            $form = ActiveForm::begin([
                'id' => 'register-form',
            ]) ?>

            <div class="register-form-item register-message-field">
                <ul>
                    <?php foreach($model->errors as $error): ?>
                            <li><?= $error[0] ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="register-form-item">
                    <label for="user-login">Введите ваш логин</label>
                    <?= $form->field($model, 'login', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'Логин', 'value'=> $model->login ])->hint('') ?>
            </div>
            <div class="register-form-item">
                    <label for="user-password">Введите свой пароль</label>
                    <?= $form->field($model, 'password', ['enableLabel'=>false, 'enableError'=>false])->passwordInput(['placeholder'=>'Пароль', 'value'=> $model->password])->hint('') ?>
            </div>
            <div class="register-form-item">
                    <label for="user-email">Введите свой email</label>
                    <?= $form->field($model, 'email', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'E-mail', 'value'=> $model->email ])->hint('') ?>
            </div>
            <div class="register-form-item">
                    <label for="user-phone">Введите свой телефон (необязательно)</label>
                    <?= $form->field($model, 'phone', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'Телефон', 'value'=> $model->phone ])->hint('') ?>
            </div>
            <div class="register-form-item">
                    <?= Html::submitButton('Отправить данные', ['class' => 'user-form-submit']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
    <?php else: ?>
    <h2>Вы уже в системе</h2>
    <div>
        <?= Html::a( 'Перейти в личный кабинет', Url::to(['/cabinet']) ); ?>
    </div>
    <?php endif; ?>
</div>
