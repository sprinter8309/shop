<?php
$this->title = 'Обновить учетные данные';

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\constants\UserConst;

use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = [
    'template' => '<li>{link}</li> ',
    'label' => 'Личный кабинет',
    'url' => [ Url::to(['/cabinet'])]
];

$this->params['breadcrumbs'][] = [
    'template' => '<span class="breadcrumbs-divide">&gt;</span> <li>{link}</li>',
    'label' => 'Обновить учетные данные'
];
?>

<div>
    <?php
        echo Breadcrumbs::widget([
            'options' => [
                    'class' => 'breadcrumbs-path'
            ],
            'homeLink'=> false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [ ]
        ]);
    ?>
</div>

<div class="update-block">
    <?php if ($model->message !== UserConst::SUCCESS_UPDATE_USER): ?>
        <h2>Обновить учетные данные</h2>
        <div class="update-form">
            <form action="/cabinet/update" name="update-form" method="POST">
            <?php
                $form = ActiveForm::begin([
                        'id' => 'update-form',
                ]) ?>
                <div class="update-form-item update-message-field">
                    <ul>
                        <?php foreach($model->errors as $error): ?>
                            <li><?= $error[0] ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="update-form-item">
                        <label for="update-name-field">Редактировать логин</label>
                        <?= $form->field($model, 'login', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'Логин', 'value'=> $model->login ])->hint('') ?>
                </div>
                <div class="update-form-item">
                        <label for="update-password-field">Редактировать пароль</label>
                        <?= $form->field($model, 'password', ['enableLabel'=>false, 'enableError'=>false])->passwordInput(['placeholder'=>'Пароль', 'value'=> ''])->hint('') ?>
                </div>
                <div class="update-form-item">
                        <label for="update-password-field">Редактировать email</label>
                        <?= $form->field($model, 'email', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'E-mail', 'value'=> $model->email ])->hint('') ?>
                </div>
                </div>
                <div class="update-form-item">
                        <label for="update-password-field">Редактировать телефон</label>
                        <?= $form->field($model, 'phone', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'Телефон', 'value'=> $model->phone ])->hint('') ?>
                </div>
                <div class="update-form-item">
                        <?= Html::submitButton('Сохранить данные', ['class' => 'user-form-submit']) ?>
                </div>
            <?php ActiveForm::end(); ?>
            </form>
        </div>

        <div class="cabinet-update-link">
                <?= Html::a( 'Вернуться назад', Url::to(['/cabinet']) ); ?>
        </div>
    <?php else: ?>
        <h2>Ваши данные были успешно отредактированы</h2>
        <div>
                <?= Html::a( 'Перейти в личный кабинет', Url::to(['/cabinet']) ); ?>
        </div>
    <?php endif; ?>
</div>
