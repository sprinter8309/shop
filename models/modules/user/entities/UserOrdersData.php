<?php

namespace app\modules\user\entities;

use app\components\dto\AbstractDto;

/**
 * DTO-класс для получения данных о заказах пользователя
 * 
 * @author Oleg Pyatin
 */
class UserOrdersData extends AbstractDto
{
    public array $user_orders;
    
    public array $orders_info;
}
