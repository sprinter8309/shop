<?php

namespace app\modules\user\forms;

use Yii;
use yii\base\Model;
use app\models\User;
use app\modules\user\services\UserService;

/**
 * Форма для выполнения входа в систему
 * 
 * @author Oleg Pyatin
 */
class LoginUserForm extends Model
{
    public function __construct(protected UserService $user_service,
                                public string $login = '',
                                public string $password = '')
    {
        $this->login = '';
        $this->password = '';
        $this->user_service = $user_service;
    }
    
    public function rules(): array
    {
        return [
            ['login', 'required', 'message' => 'Не введено имя'],
            ['password', 'required', 'message' => 'Не введен пароль'],
            [['login'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['login' => 'login']],
            [['password'], 'validateUserPassword']
        ];
    }
    
    public function validateUserPassword($attribute, $params): void
    {
        if (!$this->hasErrors()) {
            $user = $this->user_service->getUserByLogin($this->login);
            
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильный пароль');
            }
        }
    }
    
    public function loginCurrentUser(): bool
    {
        return Yii::$app->user->login($this->user_service->getUserByLogin($this->login), 0);
    }
}
