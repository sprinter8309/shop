<?php

namespace app\modules\user\repositories;

use app\models\User;
use yii\db\Query;
use app\components\repositories\BaseRepository;

/**
 * Класс репозиторий для данных пользователей
 * 
 * @author Oleg Pyatin
 */
class UserRepository extends BaseRepository
{
    public function getNextUserId(): int
    {
        return ((new Query())->from('users')->max('id')) + 1;
    }
    
    public function getUserByLogin(string $login): ?User
    {
        return User::findOne(['login'=>$login]);
    }
}
