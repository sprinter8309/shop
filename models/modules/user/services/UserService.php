<?php

namespace app\modules\user\services;

use Yii;
use yii\web\Response;
use app\models\User;
use app\modules\user\entities\UserOrdersData;
use app\modules\user\factories\UserFactory;
use app\modules\user\repositories\UserRepository;
use app\modules\user\forms\LoginUserForm;
use app\modules\user\forms\CreateUserForm;
use app\modules\user\forms\UpdateUserForm;
use app\modules\order\services\OrderService;
use app\components\constants\NavigationConst;
use app\components\constants\UserConst;

/**
 * Сервис для работы с функциональности пользователей (Личный кабинет и другие действия)
 * 
 * @author Oleg Pyatin
 */
class UserService
{
    public function __construct(public UserRepository $user_repository, public UserFactory $user_factory,
                                public OrderService $order_service)
    {
        $this->user_repository = $user_repository;
        $this->user_factory = $user_factory;
        $this->order_service = $order_service;
    }
    
    public function loginUserActions(array $request_params): LoginUserForm|Response
    {
        $login_user_form = Yii::createObject(LoginUserForm::class);
        $login_user_form->load($request_params);
        
        if ($login_user_form->validate()) {   

            $login_user_form->loginCurrentUser();
            return Yii::$app->response->redirect([NavigationConst::URL_AFTER_LOGIN]); 
        } 
        
        return $login_user_form;
    }
    
    public function registerUser(array $request_params): CreateUserForm|Response
    {
        $create_user_form = Yii::createObject(CreateUserForm::class);
        $create_user_form->load($request_params);
        
        if ($create_user_form->validate()) {   

            $new_user = $this->user_factory->createUser($create_user_form, 
                    $this->user_repository->getNextUserId());
            
            try {
                
                $this->user_repository->save($new_user); 
                $this->loginUser($new_user);
                return Yii::$app->response->redirect([NavigationConst::URL_AFTER_LOGIN]); 
                
            } catch (\Throwable) {
                $create_user_form->addError('login', Yii::t(OrderConst::ERROR_MESSAGE_REGISTER_USER));
            }
        } 
        
        return $create_user_form;
    }
    
    public function getUserOrdersData(): UserOrdersData
    {
        $user = $this->getCurrentUser();
        
        return UserOrdersData::loadFromArray([
            'user_orders'=>$user->orders,
            'orders_info'=>$this->order_service->getOrdersInfo($user->orders),
        ]);
    }
    
    public function updateUser(array $request_params): UpdateUserForm
    {
        $update_user_form = Yii::createObject(UpdateUserForm::class);
        $update_user_form->load($request_params);
        
        if ($update_user_form->validate()) { 
            
            $user_data = $this->getCurrentUser();
            $user_data->login = $update_user_form->login;
            $user_data->phone = $update_user_form->phone;
            $user_data->email = $update_user_form->email;
            $user_data->changePasswordIfIsActual($update_user_form->password);
            
            try {
                $this->user_repository->save($user_data); 
                $update_user_form->message = UserConst::SUCCESS_UPDATE_USER;
                
            } catch (\Throwable) {
                $update_user_form->addError('login', Yii::t(UserConst::ERROR_MESSAGE_UPDATE_USER));
            }
        }
        
        return $update_user_form;
    }
    
    public function getUserDataForUpdate(): UpdateUserForm
    {
        $update_user_form = Yii::createObject(UpdateUserForm::class);
        $user = $this->getCurrentUser();
        
        $update_user_form->login = $user->login;
        $update_user_form->phone = $user->phone;
        $update_user_form->email = $user->email;
        
        return $update_user_form;
    }
    
    public function loginUser(User $user): bool
    {
        return Yii::$app->user->login($user, 0);
    }
    
    public function getCurrentUser(): ?User
    {
        return Yii::$app->user->identity;
    }
    
    public function getUserByLogin(string $login): ?User
    {
        return $this->user_repository->getUserByLogin($login);
    }
    
    public function logout(): Response
    {
        Yii::$app->user->logout();
        Yii::$app->session->remove('user_name');
        return Yii::$app->response->redirect([NavigationConst::URL_AFTER_LOGOUT]);
    }
}
