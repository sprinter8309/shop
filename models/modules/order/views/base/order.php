<?php
$this->title = 'Оформление заказа';

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$number_item = 0;
?>

<?php if (!Yii::$app->user->isGuest): ?>

<?php if (!($result) && !(empty($items))): ?>
    <h2>Оформить заказ</h2>

    <div class="order-container clearfix">

        <div class="order-left-part">

            <div class="order-info">
                    Выбрано <span class="order-quantity-items"><?= $quantity_items ?></span> товаров на сумму:
                            <span class="order-full-price"><b><?php echo $total_price.".00</b> руб" ?></span>
            </div>

            <div class="order-info-help">
                    Для оформления заказа введите (или подтвердите) свои данные и нажмите кнопку "Оформить"
            </div>

            <div class="order-info-help">
                    Наш менеджер свяжется с вами
            </div>
            <?php endif; ?>

            <div class="order-form-block">
                <?php if (!empty($items)): ?>

                    <?php if (!($result)): ?>
                        <div class="order-form">
                            <?php
                                $form = ActiveForm::begin([
                                    'id' => 'order-form',
                                ]) ?>

                                <div class="order-form-item order-message-field">
                                    <ul>
                                        <?php foreach($model->errors as $error): ?>
                                                <li><?= $error[0] ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <div class="order-form-item">
                                        <label for="user-login">Введите ваше имя</label>
                                        <?= $form->field($model, 'user_name', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'Имя', 'value'=> $model->user_name ])->hint('') ?>
                                </div>
                                <div class="order-form-item">
                                        <label for="user-phone">Введите свой телефон</label>
                                        <?= $form->field($model, 'user_phone', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'Телефон', 'value'=> $model->user_phone ])->hint('') ?>
                                </div>
                                <div class="order-form-item">
                                        <label for="user-phone">Введите комментарий к заказу</label>
                                        <?= $form->field($model, 'user_comment', ['enableLabel'=>false, 'enableError'=>false])->textInput(['placeholder'=>'Комментарий', 'value'=> $model->user_comment ])->hint('') ?>
                                </div>
                                <div class="order-form-item">
                                        <?= Html::submitButton('Оформить', ['class' => 'order-form-submit']) ?>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    <?php else: ?>
                        <div class="sale-success-header">
                                Ваш заказ успешно добавлен, вы можете перейти в каталог
                        </div>
                        <div>
                                <?= Html::a( 'Перейти в каталог', Url::to(['/catalog']), ['class'=>'sale-success-return'] ); ?>
                        </div>
                    <?php endif; ?>

                <?php else: ?>
                <div class="sale-no-items-header">
                        В вашей корзине нет товаров, для оформления заказа выберите их в каталоге
                </div>
                <div>
                        <?= Html::a( 'Перейти в каталог', Url::to(['/catalog']), ['class'=>'sale-no-items-return'] ); ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="order-right-part">

            <?php if ((!(empty($items)))&& (!($result))): ?>
                <div class="order-items-info">Ваш заказ</div>

                <div class="order-item-container">
                    <div class="order-item-offset"></div>
                    <?php foreach($items as $item): ?>
                        <?php $number_item++; ?>

                        <div class="order-item-point">
                            <div class="order-item-image">
                                <div class="circle-width-container circle-width-cart-container">
                                        <?= Html::img( '/site/wb-image/'.(explode('.', $item->image)[0])) ?>
                                </div>
                            </div>
                            <div class="order-item-name">
                                <span class="order-item-name-text"><?php echo $item["name"] . ' (' . $products_cart[$item["id"]] . 'шт)'; ?></span>
                            </div>
                        </div>
                        <div class="order-item-offset"></div>

                    <?php endforeach; ?>
                </div>

                <div class="order-cart-return">
                        <?= Html::a( 'Вернуться к корзине', Url::to(['/cart']) ); ?>
                </div>

            <?php endif; ?>
        </div>
    </div>

<?php else: ?>
    <div class="sale-no-items-header">
            Вы не авторизованы, для оформления заказа войдите в систему или выполните регистрацию
    </div>
    <div>
            <?= Html::a( 'Войти в систему', Url::to(['/login']), ['class'=>'sale-no-items-return'] ); ?>
    </div>
<?php endif; ?>




