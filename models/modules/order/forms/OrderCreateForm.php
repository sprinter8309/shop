<?php

namespace app\modules\order\forms;

use yii\base\Model;
use app\components\validators\PhoneValidator;

/**
 * Модель для формы создания заказа
 * 
 * @author Oleg Pyatin
 */
class OrderCreateForm extends Model
{
    public function __construct(public string $user_name = '', 
                                public string $user_phone = '', 
                                public string $user_comment = '')
    { 
        
    }
    
    public function rules(): array
    {
        return [
            ['user_name', 'required', 'message' => 'Не введено имя'],
            ['user_phone', 'required', 'message' => 'Не введен телефон'],
            [['user_name', 'user_phone', 'user_comment'], 'trim'],
            ['user_name', 'match', 'pattern'=>'/^[A-Za-zА-Яа-я0-9ё\s,]+$/i'],
            ['user_phone', PhoneValidator::class],
        ];
    }
    
    public function attributeLabels(): array
    {
        return [
            'user_name' => 'Имя покупателя',
            'user_phone' => 'Телефон',
            'user_comment' => 'Комментарий к заказу'
        ];
    }
    
    public function scenarios(): array
    {
        return [
            self::SCENARIO_DEFAULT=>[
                'user_name',
                'user_phone',
                'user_comment',
            ]
        ];
    }
}
