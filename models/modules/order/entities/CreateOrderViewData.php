<?php

namespace app\modules\order\entities;

use app\components\dto\AbstractDto;
use app\modules\order\forms\OrderCreateForm;

/**
 * DTO-класс для хранения данных корзины
 * 
 * @author Oleg Pyatin
 */
class CreateOrderViewData extends AbstractDto
{
    public OrderCreateForm $model;
    
    public bool $result;

    public array $products_cart;
    
    public array $items;
    
    public int $quantity_items;
    
    public int $total_price;
}
