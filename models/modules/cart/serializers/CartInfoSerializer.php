<?php

namespace app\modules\cart\serializers;

use app\modules\cart\entities\CartInfo;
use app\components\serializers\AbstractProperties;

/**
 * Сериализатор для DTO корзины
 * 
 * @author Oleg Pyatin
 */
class CartInfoSerializer extends AbstractProperties
{
    public function getProperties(): array 
    {
        return [
            CartInfo::class=>[
                'products',
                'items',
                'total_price',
                'quantity',
            ]
        ];
    }
}
