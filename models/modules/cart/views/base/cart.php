<?php
$this->title = 'Корзина';

use yii\helpers\Html;
use yii\helpers\Url;

$number_item = 0;
?>

<?php if ($items): ?>

    <h2>Корзина товаров</h2>

    <div class="cart-item-container">

        <?php foreach($items as $item): ?>

            <?php $number_item++; ?>

            <div class="cart-item-point <?php if ($number_item % 2) echo 'cart-item-point-strip'; ?> clearfix">
                <div class="cart-item-id">
                        <?= $number_item ?>
                </div>
                <div class="cart-item-name">
                        <?= Html::a( $item["name"], Url::to(['/catalog/item/'.$item["id"]]) ); ?>
                </div>
                <div class="cart-item-image">
                    <div class="circle-width-container circle-width-cart-container">
                            <?= Html::a(
                                    Html::img( '/site/wb-image/'.(explode('.', $item["image"])[0])),
                                    Url::to(['/catalog/item/'.$item["id"]]) ); ?>
                    </div>
                </div>
                <div class="cart-item-price">
                        <?= $item["price"].".00 руб."  ?>
                </div>
                <div class="cart-item-quantity">
                        <?= $products[$item["id"]]." шт" ?>
                </div>
                <div class="cart-item-actions">
                    <span class="cart-item-add" item-code="<?= $item["id"] ?>">Добавить</span>
                    | <span class="cart-item-delete" item-code="<?= $item["id"] ?>">Удалить</span>
                </div>
            </div>

        <?php endforeach; ?>

        <div class="cart-item-info clearfix">
            <div class="cart-item-info-price">
                    Всего товаров: <?= $quantity ?> шт, на общую стоимость: <span class="cart-info-full-price"><?= $total_price ?></span>.00 руб.
            </div>
            <div class="cart-order-button-block">
                    <a href="/order" class="cart-order-button">Оформить заказ</a>
            </div>
        </div>
    </div>

<?php else: ?>

    <div class="sale-no-items-header">
        В вашей корзине нет товаров, для оформления заказа выберите их в каталоге
    </div>
    <div>
        <?= Html::a( 'Перейти в каталог', Url::to(['/catalog']), ['class'=>'sale-no-items-return'] ); ?>
    </div>

<?php endif; ?>
