<?php

namespace app\modules\cart\entities;

use app\components\dto\AbstractDto;

/**
 * DTO-класс для хранения данных корзины
 * 
 * @author Oleg Pyatin
 */
class CartInfo extends AbstractDto
{
    /**
     * @var array  Поле для хранения списка и количеств товаров в корзине
     */
    public array $products;
    /**
     * @var int  Список характеристик товаров в корзине
     */        
    public array $items;
    /**
     * @var array  Общая цена всех товаров
     */
    public int $total_price;
    /**
     * @var array  Общее количество товаров в корзине
     */        
    public int $quantity;
}
