<?php

namespace app\modules\cart;

use yii\base\Module;

/**
 * cart module definition class
 */
class CartModule extends Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\cart\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
