<?php

namespace app\modules\site\services;

use yii\base\Component;
use app\components\image_manager\ImageManager;
use app\modules\news\services\NewsService;

/**
 * Сервис для выполнения действий над статическими страницами и общей функциональностью сайта
 * 
 * @author Oleg Pyatin
 */
class SiteService extends Component
{
    public function __construct(protected ImageManager $image_manager, protected NewsService $news_service)
    {
        $this->image_manager = $image_manager;
        $this->news_service = $news_service;
    }

    public function getNewsImage(string $file_name): never
    {
        $this->image_manager->getWBImage('news_images/', $file_name);
    }

    public function getItemImage(string $file_name): never
    {
        $this->image_manager->getWBImage('item_images/', $file_name);
    }
    
    public function getNewsForIndexPage(): array
    {
        return $this->news_service->getNewsForIndexPage();
    }
}
