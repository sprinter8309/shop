<?php

namespace app\modules\site\controllers;

use yii\web\Controller;
use app\modules\site\services\SiteService;

/**
 * Базовый контроллер для статических страниц сайта и вспомогательной функицональности
 * 
 * @author Oleg Pyatin
 */
class BaseController extends Controller
{
    public function __construct($id, $module,
            protected SiteService $site_service,
            $config = [])
    {
        $this->site_service = $site_service;
        
        parent::__construct($id, $module, $config);
    }
    
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex(): string
    {
        return $this->render('index', [
            'news' => $this->site_service->getNewsForIndexPage() 
        ]);
    }
    
    public function actionNewsImage(string $file_name): never
    {
        $this->site_service->getNewsImage($file_name);
    }
    
    public function actionItemImage(string $file_name): never
    {
        $this->site_service->getItemImage($file_name);
    }
    
    public function actionContact(): string
    {
        return $this->render('contact');
    }
    
    public function actionPolicy(): string
    {
        return $this->render('policy');
    }

    public function actionAgreement(): string
    {
        return $this->render('user_agreement');
    }

    public function actionPayInfo(): string
    {
        return $this->render('pay_info');
    }

    public function actionDelivery(): string
    {
        return $this->render('delivery');
    }
}
