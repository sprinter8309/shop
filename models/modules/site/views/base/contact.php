<?php
$this->title = 'Контакты';
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
?>

<div class="contact-common-container clearfix">
    <div class="contact-left-block">
        <?php
            echo Menu::widget([
                'items' => [
                    ['label'=>'Контактные данные', 'url'=>['/contact']],
                    ['label'=>'Пользовательское соглашение', 'url'=>['/agreement']],
                    ['label'=>'Политика конфиденциальности', 'url'=>['/policy']],
                    ['label'=>'Сведения об оплате', 'url'=>['/pay-info']],
                ],
                'options' =>[
                    'class'=>'contact-menu'
                ]
            ]);
        ?>
    </div>
    <div class="contact-right-block">
        <div class="contact-head-name">
                Контакты
        </div>
        <div class="contact-text">
            <div class="contact-info-paragraph-name">
                    Адрес магазина:
            </div>
            <div class="contact-info-paragraph">
                    <div class="contact-map">
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A7c289afedb99be20358a33a4b199093c44e6e629679179e337f4179182b6c459&amp;source=constructor" width="600" height="400" frameborder="0"></iframe>			</div>
                    </div>
                    Киров, ул.Московская, д.103<br>
                    Часы работы: <b>Пн-Пт</b> с 09:00 до 20:00, <b>Сб-Вс</b> с 10:00 до 17:00
            <div class="contact-info-paragraph-name">
                    Телефоны
            </div>
            <div class="contact-info-paragraph">
                    Отдел продаж: <a href="tel:89003458080">+7 (900) 345 80 80</a><br>
                    Менеджер по работе с клиентами: <a href="tel:89003465050">+7 (900) 346 50 50</a>
            </div>
            <div class="contact-info-paragraph-name contact-info-paragraph-short">
                    Электронная почта
            </div>
            <div class="contact-info-paragraph">
                    <a href="mailto:shop@mega-market.ru">shop@mega-market.ru</a>
            </div>
        </div>
    </div>
</div>







