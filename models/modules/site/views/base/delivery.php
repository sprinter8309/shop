<?php
$this->title = 'Сведения о доставке';
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = [
	'template' => '<li>{link}</li> ',
	'label' => 'Контакты',
	'url' => [ Url::to(['/contact'])]
];

$this->params['breadcrumbs'][] = [
	'template' => '<span class="breadcrumbs-divide">&gt;</span> <li>{link}</li>',
	'label' => 'Сведения о доставке'
];
?>

<div class="contact-common-container clearfix">
    <div class="contact-left-block">
        <?php
            echo Menu::widget([
                'items' => [
                    ['label'=>'Контактные данные', 'url'=>['/contact']],
                    ['label'=>'Пользовательское соглашение', 'url'=>['/agreement']],
                    ['label'=>'Политика конфиденциальности', 'url'=>['/policy']],
                    ['label'=>'Сведения об оплате', 'url'=>['/pay-info']],
                ],
                'options' =>[
                    'class'=>'contact-menu'
                ]
            ]);
        ?>
    </div>
    <div class="contact-right-block">
        Сведения о доставке
    </div>
</div>