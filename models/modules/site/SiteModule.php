<?php

namespace app\modules\site;

use yii\base\Module;

/**
 * site module definition class
 */
class SiteModule extends Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\site\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
