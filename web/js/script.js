function addProduct( id ) 
{	
    var xhttp = new XMLHttpRequest();

    xhttp.open('GET', '/cart/add/' + id, true);
    xhttp.send();

    xhttp.onreadystatechange = function() {

        if ( this.readyState != 4 ) { 
                return;
        } else {	
                var head_label = document.querySelector(".head-cart-quant");
                if ( head_label ) {
                        if ( this.responseText ) {					
                                head_label.innerHTML = this.responseText;
                        }
                }	

                if (window.location.pathname === '/cart') {
                        window.location.replace('/cart');
                }

                return;
        }
    }	
}

function deleteProduct( id ) 
{	
    var xhttp = new XMLHttpRequest();

    xhttp.open('GET', '/cart/delete/' + id, true);
    xhttp.send();

    xhttp.onreadystatechange = function() {

        if ( this.readyState != 4 ) { 
                return;
        } else {	
                var head_label = document.querySelector(".head-cart-quant");
                if ( head_label ) {
                        if ( this.responseText ) {					
                                head_label.innerHTML = this.responseText;
                        }
                }	

                if (window.location.pathname === '/cart') {
                        window.location.replace('/cart');
                }

                return;
        }
    }	
}

function choiceCategory( category_code ) 
{
    var xhttp = new XMLHttpRequest();
    var body = 'category_id=' + encodedURIComponent(category);
	
    xhttp.open('POST', '/catalog', true);
    xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhttp.send(body);

    xhttp.onreadystatechange = function() {

        if ( this.readyState != 4 ) { 			
            return;
        } else {	
            alert( this.responseText );
            return;
        }
    }
}

// Кнопки пагинации
pagination_labels = null;


function changeProductList(params) 
{	
    var xhttp = new XMLHttpRequest();
    
    xhttp.open('GET', '/list/update?'+params, true);
    xhttp.send();

    xhttp.onreadystatechange = function() {

        if ( this.readyState != 4 ) { 
            
            return;
        } else {	
            
            var catalog_tile = document.querySelector(".catalog-items-tile");
            if (catalog_tile) {
                if (this.responseText) {
                    
                    catalog_tile.innerHTML = this.responseText;
                    
                    // Делаем привязку события к выбору combobox-а заново (поскольку мы полностью перезагрузили контент)
                    var pagination_choice = document.querySelector(".pagination-choice");
                    pagination_choice.onchange = function() {
                        changeProductList('type=pagination&quantity='+this.options[this.selectedIndex].value+'&path='+window.location.pathname+'/0');
                    }
                    
                    var sort_price_label = document.querySelector(".sort-choice-price");
                    var sort_rating_label = document.querySelector(".sort-choice-rating");
                    sort_price_label.onclick = function() {
                        changeProductList('type=sort&method=price&path='+window.location.pathname+'/0');
                    }
                    sort_rating_label.onclick = function() {
                        // Пока делаем id
                        changeProductList('type=sort&method=rating&path='+window.location.pathname+'/0');
                    }
                    
                    var view_type_line = document.querySelector(".pagination-view-type-line");
                    var view_type_tile = document.querySelector(".pagination-view-type-tile");
                    view_type_line.onclick = function() {
                        changeProductList('type=display&show=line&path='+window.location.pathname+'/0');
                    }
                    view_type_tile.onclick = function() {
                        changeProductList('type=display&show=tile&path='+window.location.pathname+'/0');
                    }
                    
                    
                    var filter_producer_choice = document.querySelector(".catalog-filter-choice");
                    var filter_button_run = document.querySelector(".catalog-filter-button-run");
                    filter_button_run.onclick = function() {
                        
                        // Проверяем наличие кнопки сброса фильтра - если ее нет - добавляем
                        exist_discard_button = document.querySelector(".catalog-filter-button-discard");
                        
                        if (!exist_discard_button) {
                            
                            var discard_button_block = document.querySelector(".catalog-filter-discard-buttons");
                            var discard_button = document.createElement('button');
                            discard_button.className = "catalog-filter-button-discard";
                            discard_button.innerHTML = "Сбросить фильтр";
                            discard_button.onclick = function() {
                                // Здесь делаем убирание кнопки
                                changeProductList('type=filter&discard=yes&path='+window.location.pathname+'/0');
                            }
                            // Удаляем все элементы блока кнопок (чтобы она могла быть только одна - например случай нажатия на 
                            //     кнопку фильтрации 2 раза)
                            while (discard_button_block.firstChild) {
                                discard_button_block.removeChild(discard_button_block.firstChild);
                            }
                            discard_button_block.appendChild(discard_button);
                        }
                        
                        producer_name = filter_producer_choice.value;
                        changeProductList('type=filter&producer='+producer_name+'&path='+window.location.pathname+'/0');
                    }  
                    
                    // Если был запрос сброса фильтрации - убираем кнопку сброса фильтра
                    if (params.indexOf('discard=yes') !== -1) {
                        var discard_button_block = document.querySelector(".catalog-filter-discard-buttons");
                        while (discard_button_block.firstChild) {
                            discard_button_block.removeChild(discard_button_block.firstChild);
                        }
                    }
                    
                    
                    pagination_labels = document.querySelectorAll(".pagination li > a");

                    for (var i = 0; i < (pagination_labels.length); i++) {
                        pagination_labels[i].addEventListener('click', function(event) {
                            event.preventDefault();

                            label_href = this.getAttribute('href');

                            if (label_href.indexOf('\?') !== -1) {
                                base_page_param = this.getAttribute('href').split('?')[1]; // Отсекаем по знаку ?
                                path = base_page_param.split('=')[1]; // Отсекаем сам путь
                                
                                get_params = 'path='+path;

                                changeProductList(get_params+'&ajax=true');
                            }
                        });
                    }  
                    
                    
                    // Добавляем события на кнопки покупки
                    var buttons_sale = document.querySelectorAll(".catalog-item-buy");
                    var buttons_cart_sale = document.querySelectorAll(".cart-item-add");
                    var buttons_cart_delete = document.querySelectorAll(".cart-item-delete");
                    
                    for (var i = 0; i < buttons_sale.length; i++) {
                        buttons_sale[i].onclick = function() {
                            item_code = this.getAttribute( 'item-code' );		
                            addProduct( item_code );
                        }
                    }

                    for (var i = 0; i < buttons_cart_sale.length; i++) {
                        buttons_cart_sale[i].onclick = function() {
                            item_code = this.getAttribute( 'item-code' );		
                            addProduct( item_code );
                        }
                    }	

                    for (var i = 0; i < buttons_cart_delete.length; i++) {
                        buttons_cart_delete[i].onclick = function() {
                            item_code = this.getAttribute( 'item-code' );		
                            deleteProduct( item_code );
                        }
                    }
                    
                }
            }	
               
            if (window.location.search !== '') { 
                // Переводим на первую страницу
                window.location.replace(window.location.pathname);
            }

            return;
        }
    }	
}


function initialize() 
{
    var buttons_sale = document.querySelectorAll(".catalog-item-buy");
    var buttons_cart_sale = document.querySelectorAll(".cart-item-add");
    var buttons_cart_delete = document.querySelectorAll(".cart-item-delete");
    var buttons_category = document.querySelectorAll(".catalog-index-path");
    var pagination_choice = document.querySelector(".pagination-choice");
    
    var sort_price_label = document.querySelector(".sort-choice-price");
    var sort_rating_label = document.querySelector(".sort-choice-rating");
    
    var view_type_line = document.querySelector(".pagination-view-type-line");
    var view_type_tile = document.querySelector(".pagination-view-type-tile");
    
    // Кнопка фильтрации
    var filter_producer_choice = document.querySelector(".catalog-filter-choice");
    var filter_button_run = document.querySelector(".catalog-filter-button-run");
    var filter_button_discard = document.querySelector(".catalog-filter-button-discard");

    for (var i = 0; i < buttons_sale.length; i++) {
        buttons_sale[i].onclick = function() {
            item_code = this.getAttribute( 'item-code' );		
            addProduct( item_code );
        }
    }

    for (var i = 0; i < buttons_cart_sale.length; i++) {
        buttons_cart_sale[i].onclick = function() {
            item_code = this.getAttribute( 'item-code' );		
            addProduct( item_code );
        }
    }	

    for (var i = 0; i < buttons_cart_delete.length; i++) {
        buttons_cart_delete[i].onclick = function() {
            item_code = this.getAttribute( 'item-code' );		
            deleteProduct( item_code );
        }
    }

    for (var i = 0; i < buttons_category.length; i++) {
        buttons_category[i].onclick = function() {
            category_code = this.getAttribute( 'category-code' );		
        }
    }	
    
    // Учесть необходима ли обновка 
    
    // Вызов функции при изменении количетсва выводимых элементов
    pagination_choice.onchange = function() {
        changeProductList('type=pagination&quantity='+this.options[this.selectedIndex].value+'&path='+window.location.pathname+'/0');
    }
    
    sort_price_label.onclick = function() {
        changeProductList('type=sort&method=price&path='+window.location.pathname+'/0');
    }
    sort_rating_label.onclick = function() {
        // Пока делаем id
        changeProductList('type=sort&method=rating&path='+window.location.pathname+'/0');
    }
    
    view_type_line.onclick = function() {
        changeProductList('type=display&show=line&path='+window.location.pathname+'/0');
    }
    view_type_tile.onclick = function() {
        changeProductList('type=display&show=tile&path='+window.location.pathname+'/0');
    }
    
    pagination_labels = document.querySelectorAll(".pagination li > a");

    for (var i = 0; i < (pagination_labels.length); i++) {
        pagination_labels[i].addEventListener('click', function(event) {
            event.preventDefault();
            
            label_href = this.getAttribute('href');
            
            if (label_href.indexOf('\?') !== -1) {

                base_page_param = this.getAttribute('href').split('?')[1]; // Отсекаем по знаку ?
                path = base_page_param.split('=')[1]; // Отсекаем сам путь
                get_params = 'path='+path;

                changeProductList(get_params+'&ajax=true');
            }
        });
    } 

    
    // Запускаем фильтрацию
    filter_button_run.onclick = function() {
        
        // Создаем кнопку сброса фильтров
        var discard_button_block = document.querySelector(".catalog-filter-discard-buttons");
        var discard_button = document.createElement('button');
        discard_button.className = "catalog-filter-button-discard";
        discard_button.innerHTML = "Сбросить фильтр";
        discard_button.onclick = function() {
            // Здесь делаем убирание кнопки
            changeProductList('type=filter&discard=yes&path='+window.location.pathname+'/0');
        }
        while (discard_button_block.firstChild) {
            discard_button_block.removeChild(discard_button_block.firstChild);
        }
        discard_button_block.appendChild(discard_button);
     
        producer_name = filter_producer_choice.value;
        changeProductList('type=filter&producer='+producer_name+'&path='+window.location.pathname+'/0');
    }
}

document.addEventListener("DOMContentLoaded", initialize);


